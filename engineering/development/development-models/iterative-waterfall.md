---
title: Iterative waterfall
description: Iterative waterfall software development model.
published: true
date: 2021-12-20T14:21:15.883Z
tags: category - development, category - development-models, page type - content, section - engineering, topic - iterative-waterfall
editor: markdown
dateCreated: 2021-12-19T11:58:30.261Z
---

# Overview

> Adapted from https://www.geeksforgeeks.org/software-engineering-iterative-waterfall-model/.
{.is-info}

In a practical software development project, the [classical waterfall model](/en/engineering/development/development-models/classical-waterfall) is hard to use. So, the Iterative waterfall model can be thought of as incorporating the necessary changes to the classical waterfall model to make it usable in practical software development projects. It is almost the same as the classical waterfall model except some changes are made to increase the efficiency of the software development.

# Phases and details

The iterative waterfall model provides feedback paths from every phase to its preceding phases, which is the main difference from the classical waterfall model. 

Feedback paths introduced by the iterative waterfall model are shown in the figure below.

```mermaid
graph TB
  1[Feasibility study]
    --> 2[Requirement analysis]
    --> 3[Design]
    --> 4[Coding and unit testing]
    --> 5[Integration and unit testing]
    --> 6[Maintenance]

  %% Backflow
  6 -- Review/Iterate --> 2
  6 -- Review/Iterate --> 3
  6 -- Review/Iterate --> 4
  6 -- Review/Iterate --> 5
```

When errors are detected at some later phase, these feedback paths allow correcting errors committed by programmers during some phase. The feedback paths allow the phase to be reworked in which errors are committed and these changes are reflected in the later phases. But, there is no feedback path to the stage – feasibility study, because once a project has been taken, does not give up the project easily. 

It is good to detect errors in the same phase in which they are committed. It reduces the effort and time required to correct the errors. 

**Phase Containment of Errors :**
The principle of detecting errors as close to their points of commitment as possible is known as Phase containment of errors. 

 

# Advantages

- **Feedback Path**
    In the classical waterfall model, there are no feedback paths, so there is no mechanism for error correction. But in the iterative waterfall model feedback path from one phase to its preceding phase allows correcting the errors that are committed and these changes are reflected in the later phases.
- **Simple**
    Iterative waterfall model is very simple to understand and use. That’s why it is one of the most widely used software development models.
- **Cost-Effective**
    It is highly cost-effective to change the plan or requirements in the model. Moreover, it is best suited for agile organizations.
- **Well-organized**
    In this model, less time is consumed on documenting and the team can spend more time on development and designing.
{.grid-list}
     

# Drawbacks

- **Difficult to incorporate change requests**
    The major drawback of the iterative waterfall model is that all the requirements must be clearly stated before starting the development phase. Customers may change requirements after some time but the iterative waterfall model does not leave any scope to incorporate change requests that are made after the development phase starts.
- **Incremental delivery not supported**
    In the iterative waterfall model, the full software is completely developed and tested before delivery to the customer. There is no scope for any intermediate delivery. So, customers have to wait a long for getting the software.
- **Overlapping of phases not supported**
    Iterative waterfall model assumes that one phase can start after completion of the previous phase, But in real projects, phases may overlap to reduce the effort and time needed to complete the project.
- **Risk handling not supported**
    Projects may suffer from various types of risks. But, the Iterative waterfall model has no mechanism for risk handling.
- **Limited customer interactions**
    Customer interaction occurs at the start of the project at the time of requirement gathering and at project completion at the time of software delivery. These fewer interactions with the customers may lead to many problems as the finally developed software may differ from the customers’ actual requirements.
{.grid-list}
