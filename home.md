---
title: Welcome to vdSHOP docs
description: vdSHOP docs home page.
published: true
date: 2021-12-20T14:16:38.804Z
tags: page type - content, topic - home
editor: markdown
dateCreated: 2021-10-13T11:06:58.047Z
---

Find here the available documentation about vdSHOP public projects, Docker images, services, software and more.

# Browse by content

- [Engineering *Generic resources related with Engineering.*](/en/engineering)
- [Languages *Languages generic documentation and resources.*](/en/languages)
- [Platforms *Platforms documentation.*](/en/platforms)
- [Projects *Projects documentation.*](/en/projects)
- [Services *Services documentation.*](/en/services)
{.links-list}

# :thumbsup: Thumbs-up rules

- :mag: Use **Search bar**, [**Tag search**](/t), `Browse by content` sections and breadcrumbs to navigate and find content.
- :eyeglasses: You may get a better visualization experience by adjusting your **browser zoom to 80%**, depending on your screen size.
{.grid-list}

# :copyright: License

![Creative Commons license](https://i.creativecommons.org/l/by/4.0/88x31.png)

All content in this site is available under the [Creative Commons Attribution License](http://creativecommons.org/licenses/by/4.0/).
