---
title: Cheat sheets
description: Markdown cheat sheets.
published: true
date: 2021-12-19T02:12:10.119Z
tags: category - markdown, page type - content, section - languages, topic - cheat-sheets
editor: markdown
dateCreated: 2021-12-03T14:41:50.185Z
---

It's highly recommended checking these handy cheat sheets to boost your work:

- [Basic *Basic syntax cheat sheet*](https://www.markdownguide.org/cheat-sheet/#basic-syntax)
- [Extended *Extended syntax cheat sheet*](https://www.markdownguide.org/cheat-sheet/#extended-syntax)
{.links-list}

# Markdown in Wiki.js

> :thumbsup: Remember to read also [Markdown in Wiki.js: cheat sheets and user guide](/en/platforms/wiki-js/markdown) to find all what is possible to do within Wiki.js.
{.is-success}
