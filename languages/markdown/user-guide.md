---
title: User guide
description: Markdown syntax and user guides.
published: true
date: 2021-12-19T02:12:10.134Z
tags: category - markdown, page type - content, section - languages, topic - user-guide
editor: markdown
dateCreated: 2021-12-03T14:41:53.491Z
---

# What is Markdown?

> **Markdown is a lightweight markup language with plain text formatting syntax.**
{.is-info}

<br>

## In a nutshell

Markdown can be defined as a **plain text formatting syntax** used to write content on the web. It’s commonly used by writers and programmers to write quickly without having to take time using the formatting toolbar of text editors.

**Here’s Markdown in action:**

```
Are you a **dog** person *or* a **cat** person?
```

It gets converted into:

Are you a **dog** person *or* a **cat** person?

In this example, `*` wrapped around a word/phrase makes that text *italic*, and `**` wrapped around a word/phrase makes that text **bold**.

In other words, **Markdown is a quick and easy way to format plain text using special characters** like asterisks, dashes, underscores, etc. Markdown enables you to create headers, lists, code blocks, and more without lifting your fingers off your keyboard.

Its sheer simplicity and minimalist approach to writing and formatting is the reason why Markdown has grown in popularity over the years, especially amongst writers and publishers. It has found a cult following of those who swear by it for quick publishing.

<br>

## Further info

- [Blog BIT.AI *What is Markdown & How It Can Help You Write Faster?*](https://blog.bit.ai/what-is-markdown/)
- [Ultraedit *What is Markdown, and why should you use it?*](https://www.ultraedit.com/company/blog/community/what-is-markdown-why-use-it.html)
{.links-list}

# User guides

:book: Learn basic and advanced features with these documentation links:

- [Basic *Basic syntax user guide*](https://www.markdownguide.org/basic-syntax/)
- [Extended *Extended syntax user guide*](https://www.markdownguide.org/extended-syntax/)
{.links-list}

# Markdown in Wiki.js

> :thumbsup: Remember to read also [Markdown in Wiki.js: cheat sheets and user guide](/en/platforms/wiki-js/markdown) to find all what is possible to do within Wiki.js.
{.is-success}
