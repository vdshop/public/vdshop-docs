---
title: PHP
description: PHP directory index.
published: true
date: 2021-12-19T02:12:10.083Z
tags: page type - index, section - languages, topic - php
editor: markdown
dateCreated: 2021-12-19T02:12:09.310Z
---

{.directory-index-start }

# Browse by content

- [PHP 7.3 *PHP 7.3 directory index.*](/en/languages/php/v7-3)
- [PHP 7.4 *PHP 7.4 directory index.*](/en/languages/php/v7-4)
- [PHP 8.0 *PHP 8.0 directory index.*](/en/languages/php/v8-0)
- [PHP 8.1 *PHP 8.1 directory index.*](/en/languages/php/v8-1)
{.links-list}

# Browse by tags

[Browse related content by tags](/t/category%20-%20php/page%20type%20-%20content/section%20-%20languages/?sort=title).

{.directory-index-end }
