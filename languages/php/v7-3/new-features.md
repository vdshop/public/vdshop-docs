---
title: New features
description: PHP 7.3 new features.
published: true
date: 2021-12-20T11:23:08.788Z
tags: category - php, category - v7-3, page type - content, section - languages, topic - new-features
editor: markdown
dateCreated: 2021-12-13T18:03:52.151Z
---

{.include-start https://raw.githubusercontent.com/brendt/stitcher.io/master/src/content/blog/2018-08-20-new-in-php-73.md}

> Content adapted from https://raw.githubusercontent.com/brendt/stitcher.io/master/src/content/blog/2018-08-20-new-in-php-73.md
{.is-info}

# New in PHP 7.3

- Released on December 6, 2018
- [Trailing commas](#trailing-commas-in-function-calls-rfc) are now allowed in function calls
- The [`is_countable`](#is_countable-rfc) function
- Improvements to the [Heredoc syntax](#flexible-heredoc-syntax-rfc) makes it more flexible to use
- [`array_key_first` and `array_key_last`](#array_key_first-and-array_key_last-rfc) are two new array helper functions

<br>

## `is_countable` [rfc](https://wiki.php.net/rfc/is-countable)

PHP 7.2 added a warning when counting uncountable objects.
The `is_countable` function can help prevent this warning.

```php
$count = is_countable($variable) ? count($variable) : null;
```

<br>

## `array_key_first` and `array_key_last` [rfc](https://wiki.php.net/rfc/array_key_first_last)

These two functions basically do what the name says.

```php
$array = [
    'a' => '…',
    'b' => '…',
    'c' => '…',
];

array_key_first($array); // 'a'
array_key_last($array); // 'c'
```

The original RFC also proposed `array_value_first` and `array_value_last`,
but these were voted against by the majority of people.

Another idea for `array_first` and `array_last` was proposed which would return a tuple `[$key => $value]`,
but opinions were mixed.
For now we only have two functions to get the first and last key of an array.

<br>

## Flexible Heredoc syntax [rfc](https://wiki.php.net/rfc/flexible_heredoc_nowdoc_syntaxes)

Heredoc can be a useful tool for larger strings, though they had an indentation quirk in the past.

```php
// Instead of this:

$query = <<<SQL
SELECT *
FROM `table`
WHERE `column` = true;
SQL;

// You can do this:

$query = <<<SQL
    SELECT *
    FROM `table`
    WHERE `column` = true;
    SQL;
```

This is especially useful when you're using Heredoc in an already nested context.

The whitespaces in front of the closing marker will be ignored on all lines.

An important note: because of this change, some existing Heredocs might break,
when they are using the same closing marker in their body.

```php
$str = <<<FOO
abcdefg
    FOO
FOO;

// Parse error: Invalid body indentation level in PHP 7.3
```

<br>

## Trailing commas in function calls [rfc](https://wiki.php.net/rfc/trailing-comma-function-calls)

What was already possible with arrays, can now also be done with function calls.
Note that it's not possible in function definitions!

```php
$compacted = compact(
    'posts',
    'units',
);
```

<br>

## Better type error reporting

`TypeErrors` for integers and booleans used to print out their full name,
it has been changed to `int` and `bool`, to match the type hints in the code.

```txt
Argument 1 passed to foo() must be of the type int/bool
```

In comparison to PHP 7.2:

```txt
Argument 1 passed to foo() must be of the type
integer/boolean
```

<br>

## JSON errors can be thrown [rfc](https://wiki.php.net/rfc/json_throw_on_error)

Previously, JSON parse errors were a hassle to debug.
The JSON functions now accept an extra option to make them throw an exception on parsing errors.
This change obviously adds a new exception: `JsonException`.

```php
json_encode($data, JSON_THROW_ON_ERROR);

json_decode("invalid json", null, 512, JSON_THROW_ON_ERROR);

// Throws JsonException
```

While this feature is only available with the newly added option,
there's a chance it'll be the default behaviour in a future version.

<br>

## `list` reference assignment [rfc](https://wiki.php.net/rfc/list_reference_assignment)

The `list()` and its shorthand `[]` syntax now support references.

```php
$array = [1, 2];

list($a, &$b) = $array;

$b = 3;

// $array = [1, 3];
```

<br>

## Undefined variables in `compact` [rfc](https://wiki.php.net/rfc/compact)

Undefined variables passed to `compact` will be reported with a notice, they were previously ignored.

```php
$a = 'foo';

compact('a', 'b');

// Notice: compact(): Undefined variable: b
```

<br>

## Case-insensitive constants [rfc](https://wiki.php.net/rfc/case_insensitive_constant_deprecation)

There were a few edge cases were case-insensitive constants were allowed.
These have been deprecated.

<br>

## Same site cookie [rfc](https://wiki.php.net/rfc/same-site-cookie)

This change not only adds a new parameter,
it also changes the way the `setcookie`, `setrawcookie` and `session_set_cookie_params` functions work in a non-breaking manner.

Instead of one more parameters added to already huge functions, they now support an array of options, whilst still being backwards compatible.
An example:

```
bool setcookie(
    string $name
    [, string $value = ""
    [, int $expire = 0
    [, string $path = ""
    [, string $domain = ""
    [, bool $secure = false
    [, bool $httponly = false ]]]]]]
)

bool setcookie (
    string $name
    [, string $value = ""
    [, int $expire = 0
    [, array $options ]]]
)

// Both ways work.
```

<br>

## PCRE2 migration [rfc](https://wiki.php.net/rfc/pcre2-migration)

PCRE —&thinsp;short for "Perl Compatible Regular Expressions"&thinsp;— has been updated to v2.

The migration had a focus on maximum backwards compatibility, though there are a few breaking changes.
Be sure to read the [RFC](https://wiki.php.net/rfc/pcre2-migration) to know about them.

<br>

## String search functions [README](https://github.com/php/php-src/blob/43329e85e682bed4919bb37c15acb8fb3e63175f/UPGRADING#L327-L339)

You can no longer pass a non-string needle to string search functions.
These are the affected functions:

```php
strpos()
strrpos()
stripos()
strripos()
strstr()
strchr()
strrchr()
stristr()
```

<br>

## MBString updates [README](https://github.com/php/php-src/blob/php-7.3.0RC6/UPGRADING#L186-L232)

`MBString` is PHP's way of [handling complex strings](http://php.net/manual/en/intro.mbstring.php).
This module has received some updates in this version of PHP.
You can read about it [here](https://github.com/php/php-src/blob/php-7.3.0RC6/UPGRADING#L186-L232).

<br>

## Several deprecations [rfc](https://wiki.php.net/rfc/deprecations_php_7_3)

Several small things have been deprecated, there's a possibility errors can show up in your code because of this.

- Undocumented `mbstring` function aliases
- String search functions with integer needle
- `fgetss()` function and `string.strip_tags` filter
- Defining a free-standing `assert()` function
- `FILTER_FLAG_SCHEME_REQUIRED` and `FILTER_FLAG_HOST_REQUIRED` flags
- `pdo_odbc.db2_instance_name` php.ini directive

Please refer to the [RFC](https://wiki.php.net/rfc/deprecations_php_7_3) for a full explanation of each deprecation.

{.include-end https://raw.githubusercontent.com/brendt/stitcher.io/master/src/content/blog/2018-08-20-new-in-php-73.md}
