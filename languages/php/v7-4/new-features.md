---
title: New features
description: PHP 7.4 new features.
published: true
date: 2021-12-20T11:23:09.074Z
tags: category - php, category - v7-4, page type - content, section - languages, topic - new-features
editor: markdown
dateCreated: 2021-12-13T18:03:18.822Z
---

{.include-start https://raw.githubusercontent.com/brendt/stitcher.io/master/src/content/blog/2019-01-01-new-in-php-74.md}

> Content adapted from https://raw.githubusercontent.com/brendt/stitcher.io/master/src/content/blog/2019-01-01-new-in-php-74.md
{.is-info}

# What's new in PHP 7.4

PHP 7.4 is the latest stable version of PHP. It was released on November 28, 2019 and it's the last version before [PHP 8](/blog/new-in-php-8). It brings lots of new features, syntax additions and fixes. In this post you'll find a list with everything that's new and changed to help you prepare for the upgrade. Let's start though with a few highlights, included in PHP 7.4:

- [Arrow functions](#arrow-functions-rfc) for cleaner one-liner functions
- [Preloading](#preloading-rfc) to improve performance
- [Typed properties](#typed-properties-rfc) in classes
- [Improved type variance](#improved-type-variance-rfc)
- The [null coalescing assignment operator](#null-coalescing-assignment-operator-rfc) as a shorthand
- [FFI](#foreign-function-interface-rfc) for better extension development in PHP
- Underscores can be used to [format numeric values](#numeric-literal-separator-rfc)
- Spread operator [in arrays](#array-spread-operator-rfc)
- And more, you can read about it here

<br>

## New features

PHP 7.4 comes with a remarkable amount of new features. We'll start with a list of all new features, and then look at changes and deprecations.

A note before we dive in though: if you're still on a lower version of PHP,
you'll also want to read what's [new in PHP 7.3](/blog/new-in-php-73).

---

<br>

### Arrow functions [rfc](https://wiki.php.net/rfc/arrow_functions_v2)

Arrow functions, also called "short closures", allow for less verbose one-liner functions.

While you'd previously write this:

```php
array_map(function (User $user) {
    return $user->id;
}, $users)
```

You can now write this:

```php
array_map(fn (User $user) => $user->id, $users)
```

There are a few notes about arrow functions:

- They can always access the parent scope, there's no need for the `use` keyword.
- `$this` is available just like normal closures.
- Arrow functions may only contain one expression, which is also the return statement.

You can read about them in depth [here](/blog/short-closures-in-php).

---

---

<br>

### Typed properties [rfc](https://wiki.php.net/rfc/typed_properties_v2)

Class variables can be type hinted:

```php
class A
{
    public string $name;

    public ?Foo $foo;
}
```

There's lots to tell about this feature, so I wrote a [dedicated post](/blog/typed-properties-in-php-74) about them.

---

<br>

### Improved type variance [rfc](https://wiki.php.net/rfc/covariant-returns-and-contravariant-parameters)

I also wrote about PHP's type system in [the past](/blog/what-php-can-be),
so it's good to see some improvements are actually arriving in PHP's core.

Type variance is another topic worth its [own blog post](/blog/liskov-and-type-safety),
but in short: you'll be able use covariant return types –

```php
class ParentType {}
class ChildType extends ParentType {}

class A
{
    public function covariantReturnTypes(): ParentType
    { /* … */ }
}

class B extends A
{
    public function covariantReturnTypes(): ChildType
    { /* … */ }
}
```

– and contravariant arguments.

```php
class A
{
    public function contraVariantArguments(ChildType $type)
    { /* … */ }
}

class B extends A
{
    public function contraVariantArguments(ParentType $type)
    { /* … */ }
}
```

<br>

### Null coalescing assignment operator [rfc](https://wiki.php.net/rfc/null_coalesce_equal_operator)

Next is the null coalescing assignment operator, a shorthand for null coalescing operations. Instead of doing this:

```php
$data['date'] = $data['date'] ?? new DateTime();
```

You can do this:

```php
$data['date'] ??= new DateTime();
```

You can read more about this operator in my post about [PHP shorthands](/blog/shorthand-comparisons-in-php).

---

<br>

### Array spread operator [rfc](https://wiki.php.net/rfc/spread_operator_for_array)

Next up, it's now possible to use the spread operator in arrays:

```php
$arrayA = [1, 2, 3];

$arrayB = [4, 5];

$result = [0, ...$arrayA, ...$arrayB, 6 ,7];

// [0, 1, 2, 3, 4, 5, 6, 7]
```

Note that this only works with arrays with numerical keys, as of PHP 8.1 it's also possible to [unpack arrays with string keys](/blog/new-in-php-81#array-unpacking-with-string-keys-rfc).

---

<br>

### Numeric Literal Separator [rfc](https://wiki.php.net/rfc/numeric_literal_separator)

PHP 7.4 allows for underscores to be used to visually separate numeric values.
It looks like this:

```php
$unformattedNumber = 107925284.88;

$formattedNumber = 107_925_284.88;
```

The underscores are simply ignored by the engine.

---

<br>

### Foreign function interface [rfc](https://wiki.php.net/rfc/ffi)

Moving on to some more core-level features: foreign function interface or "FFI" in short, allows us to call C code from userland.
This means that PHP extensions could be written in pure PHP and loaded via composer.

It should be noted though that this is a complex topic.
You still need C knowledge to be able to properly use this feature.

---

<br>

### Preloading [rfc](https://wiki.php.net/rfc/preload)

Another lower-level feature is preloading.
It's is an amazing addition to PHP's core,
which can result in some significant performance improvements.

In short: if you're using a framework,
its files have to be loaded and linked on every request.
Preloading allows the server to load PHP files in memory on startup,
and have them permanently available to all subsequent requests.

The performance gain comes of course with a cost:
if the source of preloaded files are changed, the server has to be restarted.

Do you want to know more? I wrote a dedicated post about [setting up and using preloading](/blog/preloading-in-php-74) and have also done some [preloading benchmarks](/blog/php-preload-benchmarks).

---

<br>

### Custom object serialization [rfc](https://wiki.php.net/rfc/custom_object_serialization)

Two new magic methods have been added: `__serialize` and `__unserialize`.
The difference between these methods and `__sleep` and `__wakeup` is discussed in the RFC.

---

<br>

### Reflection for references [rfc](https://wiki.php.net/rfc/reference_reflection)

Libraries like Symfony's var dumper rely heavily on the reflection API to reliably dump a variable.
Previously it wasn't possible to properly reflect references,
resulting in these libraries relying on hacks to detect them.

PHP 7.4 adds the `ReflectionReference` class which solves this issue.

---

<br>

### Weak references [rfc](https://wiki.php.net/rfc/weakrefs)

Weak references are references to objects, which don't prevent them from being destroyed.

---

<br>

### `mb_str_split` added [rfc](https://wiki.php.net/rfc/mb_str_split)

This function provides the same functionality as `str_split`, but on multi-byte strings.

---

<br>

### Password Hashing Registry [rfc](https://wiki.php.net/rfc/password_registry)

Internal changes have been made to how hashing libraries are used, so that it's easier for userland to use them.

More specifically, a new function `password_algos` has been added which returns a list of all registered password algorithms.

The RFC was a little unclear about the benefits, luckily Sara was able to provide [some more context](https://www.reddit.com/r/PHP/comments/e4uqha/whats_new_in_php_74/f9gafhv/):

> It means that ext/sodium (or anyone really) can register password hashing algorithms dynamically. The upshot of which is that argon2i and argon2id will be more commonly available moving forward

---

<br>

## Changes and deprecations

Besides new features, there are also lots of changes to the language. Most of these changes are non-breaking, though some might have an effect on your code bases.

Note that deprecation warnings aren't per definition "breaking", but merely a notice to the developer that functionality will be removed or changed in the future. It would be good not to ignore deprecation warnings, and to fix them right away; as it will make the upgrade path for PHP 8.0 more easy.

---

<br>

### Left-associative ternary operator deprecation [rfc](https://wiki.php.net/rfc/ternary_associativity)

The ternary operator has [some weird quirks](/blog/shorthand-comparisons-in-php#ternary-operator) in PHP.
This RFC adds a deprecation warning for nested ternary statements.
In PHP 8, this deprecation will be converted to a compile time error.

```php
1 ? 2 : 3 ? 4 : 5; // deprecated
(1 ? 2 : 3) ? 4 : 5; // ok
```

---

<br>

### Exceptions allowed in `__toString` [rfc](https://wiki.php.net/rfc/tostring_exceptions)

Previously, exceptions could not be thrown in `__toString`.
They were prohibited because of a workaround for some old core error handling mechanisms,
but Nikita pointed out that this "solution" didn't actually solve the problem it tried to address.

This behaviour is now changed, and exceptions can be thrown from `__toString`.

---

<br>

### Concatenation precedence [rfc](https://wiki.php.net/rfc/concatenation_precedence)

If you'd write something like this:

```php
echo "sum: " . $a + $b;
```

PHP would previously interpret it like this:

```php
echo ("sum: " . $a) + $b;
```

PHP 8 will make it so that it's interpreted like this:

```php
echo "sum: " . ($a + $b);
```

PHP 7.4 adds a deprecation warning when encountering an unparenthesized expression containing a `.` before a `+` or `-` sign.

---

<br>

### `array_merge` without arguments [upgrading](https://github.com/php/php-src/blob/PHP-7.4/UPGRADING#L276)

Since the addition of the spread operator, there might be cases where you'd want to use `array_merge` like so:

```php
$merged = array_merge(...$arrayOfArrays);
```

To support the edge case where `$arrayOfArrays` would be empty, both `array_merge` and `array_merge_recursive` now allow an empty parameter list.
An empty array will be returned if no input was passed.

---

<br>

### Curly brackets for array and string access [rfc](https://wiki.php.net/rfc/deprecate_curly_braces_array_access)

It was possible to access arrays and string offsets using curly brackets:

```php
$array{1};
$string{3};
```

This has been deprecated.

---

<br>

### Invalid array access notices [rfc](https://wiki.php.net/rfc/notice-for-non-valid-array-container)

If you were to use the array access syntax on, say, an integer; PHP would previously return `null`.
As of PHP 7.4, a notice will be emitted.

```php
$i = 1;

$i[0]; // Notice
```

---

<br>

### `proc_open` improvements [upgrading](https://github.com/php/php-src/blob/PHP-7.4/UPGRADING#L319)

Changes were made to `proc_open` so that it can execute programs without going through a shell. This is done by passing an array instead of a string for the command.

---

<br>

### `strip_tags` also accepts arrays [upgrading](https://github.com/php/php-src/blob/PHP-7.4/UPGRADING#L259)

You used to only be able to strip multiple tags like so:

```php
strip_tags($string, '<a><p>')
```

PHP 7.4 also allows the use of an array:

```php
strip_tags($string, ['a', 'p'])
```

---

<br>

### `ext-hash` always enabled [rfc](https://wiki.php.net/rfc/permanent_hash_ext)

This extension is now permanently available in all PHP installations.

---

<br>

### PEAR not enabled by default [externals](https://externals.io/message/103977)

Because PEAR isn't actively maintained anymore, the core team decided to remove its default installation with PHP 7.4.

---

<br>

### Several small deprecations [rfc](https://wiki.php.net/rfc/deprecations_php_7_4)

This RFC bundles lots of small deprecations, each with their own vote.
Be sure to read a more detailed explanation on the RFC page, though here's a list of deprecated things:

- The `real` type
- Magic quotes legacy
- `array_key_exists()` with objects
- `FILTER_SANITIZE_MAGIC_QUOTES` filter
- Reflection `export()` methods
- `mb_strrpos()` with encoding as 3rd argument
- `implode()` parameter order mix
- Unbinding `$this` from non-static closures
- `hebrevc()` function
- `convert_cyr_string()` function
- `money_format()` function
- `ezmlm_hash()` function
- `restore_include_path()` function
- `allow_url_include` ini directive

---

<br>

### Other changes [upgrading](https://github.com/php/php-src/blob/PHP-7.4/UPGRADING)

You should always take a look at the full [UPGRADING document](https://github.com/php/php-src/blob/PHP-7.4/UPGRADING)
when upgrading PHP versions.

---

Here are some changes highlighted:

- Calling `parent::` in a class without a parent is deprecated.
- Calling `var_dump` on a `DateTime` or `DateTimeImmutable` instance will no longer
leave behind accessible properties on the object.
- `openssl_random_pseudo_bytes` will throw an exception in error situations.
- Attempting to serialise a `PDO` or `PDOStatement` instance will generate
an `Exception` instead of a `PDOException`.
- Calling `get_object_vars()` on an `ArrayObject` instance will return
the properties of the `ArrayObject` itself, and not the values of the wrapped array or object.
Note that `(array)` casts are not affected.
- `ext/wwdx` has been deprecated.

---

<br>

### RFC voting process improvements

This is technically not an update related to PHP 7.4, though it's worth mentioning:
the voting rules for RFC's have been changed.

- They always need a [2/3 majority](https://wiki.php.net/rfc/abolish-narrow-margins) in order to pass.
- There are not more short voting periods, all RFCs must be open for at least [2 weeks](https://wiki.php.net/rfc/abolish-short-votes).

---

{.include-end https://raw.githubusercontent.com/brendt/stitcher.io/master/src/content/blog/2019-01-01-new-in-php-74.md}
