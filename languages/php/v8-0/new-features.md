---
title: New features
description: PHP 8.0 new features
published: true
date: 2021-12-20T11:23:09.560Z
tags: category - php, category - v8-0, page type - content, section - languages, topic - new-features
editor: markdown
dateCreated: 2021-12-13T12:51:06.960Z
---

{.include-start https://raw.githubusercontent.com/brendt/stitcher.io/master/src/content/blog/2020-01-16-new-in-php-8.md}

> Content adapted from https://raw.githubusercontent.com/brendt/stitcher.io/master/src/content/blog/2020-01-16-new-in-php-8.md
{.is-info}

PHP 8 was released on November 26, 2020. You can [download it here](https://www.php.net/releases/8.0/en.php). It's a new major version, which means that there are some breaking changes, as well as lots of new features and performance improvements.

Because of the breaking changes, there's a higher chance you'll need to make some changes in your code to get it running on PHP 8. If you've kept up to date with the latest releases though, the upgrade shouldn't be too hard, since most breaking changes were deprecated before in the 7.* versions. And don't worry, all these deprecations are listed in this post.

Besides breaking changes, PHP 8 also brings a nice set of new features such as [the JIT compiler](#jit-rfc), [union types](#union-types-rfc), [attributes](#attributes-rfc), and more.

<br>

## New features

Let's start with all new features, it's quite a list!

---

<br>

### Union types [RFC](https://wiki.php.net/rfc/union_types_v2)

Given the dynamically typed nature of PHP, there are lots of cases where union types can be useful. Union types are a collection of two or more types which indicate that either one of those can be used.

```php
public function foo(Foo|Bar $input): int|float;
```

Note that `void` can never be part of a union type, since it indicates "no return value at all". Furthermore, `nullable` unions can be written using `|null`, or by using the existing `?` notation:

```php
public function foo(Foo|null $foo): void;

public function bar(?Bar $bar): void;
```

<br>

### JIT [RFC](https://wiki.php.net/rfc/jit)

The JIT — just in time — compiler promises significant performance improvements, albeit not always within the context of web requests. I've done my [own benchmarks](/blog/jit-in-real-life-web-applications) on real-life web applications, and it seems like the JIT doesn't make that much of a difference, if any, on those kinds of PHP projects.

If you want to know more about what the JIT can do for PHP, you can read another post I wrote about it [here](/blog/php-jit).

---

<br>

### The nullsafe operator [RFC](https://wiki.php.net/rfc/nullsafe_operator)

If you're familiar with the [null coalescing operator](/blog/shorthand-comparisons-in-php#null-coalescing-operator) you're already familiar with its shortcomings: it doesn't work on method calls. Instead you need intermediate checks, or rely on `optional` helpers provided by some frameworks:

```php
$startDate = $booking->getStartDate();

$dateAsString = $startDate ? $startDate->asDateTimeString() : null;
```

With the addition of the nullsafe operator, we can now have null coalescing-like behaviour on methods!

```php
$dateAsString = $booking->getStartDate()?->asDateTimeString();
```

You can read all about the nullsafe operator [here](/blog/php-8-nullsafe-operator).

---

<br>

### Named arguments [RFC](https://wiki.php.net/rfc/named_params)

[Named arguments](/blog/php-8-named-arguments) allow you to pass in values to a function, by specifying the value name, so that you don't have to take their order into consideration, and you can also skip optional parameters!

```php
function foo(string $a, string $b, ?string $c = null, ?string $d = null)
{ /* … */ }

foo(
    b: 'value b',
    a: 'value a',
    d: 'value d',
);
```

You can read about them in-depth [in this post](/blog/php-8-named-arguments).

---

<br>

### Attributes [RFC](https://wiki.php.net/rfc/attributes_v2)

[Attributes](/blog/attributes-in-php-8), commonly known as _annotations_ in other languages, offers a way to add meta data to classes, without having to parse docblocks.

As for a quick look, here's an example of what attributes look like, from the RFC:

```php
use App\Attributes\ExampleAttribute;

#[ExampleAttribute]

class Foo
{
    #[ExampleAttribute]
    public const FOO = 'foo';

    #[ExampleAttribute]
    public $x;

    #[ExampleAttribute]
    public function foo(#[ExampleAttribute] $bar) { }
}
```

```php

#[Attribute]

class ExampleAttribute
{
    public $value;

    public function __construct($value)
    {
        $this->value = $value;
    }
}
```

Note that this base `Attribute` used to be called `PhpAttribute` in the original RFC, but was changed with [another RFC](https://wiki.php.net/rfc/attribute_amendments) afterwards. If you want to take a deep dive in how attributes work, and how you can build your own; you can read about [attributes in depth](/blog/attributes-in-php-8) on this blog.

<br>

### Match expression [RFC](https://wiki.php.net/rfc/match_expression_v2)

You could call it the big brother of the `switch` expression: `match` can return values, doesn't require `break` statements, can combine conditions, uses strict type comparisons and doesn't do any type coercion.

It looks like this:

```php
$result = match($input) {
    0 => "hello",
    '1', '2', '3' => "world",
};
```

You can read up on the match expression in detail, [over here](/blog/php-8-match-or-switch).

---

<br>

### Constructor property promotion [RFC](https://wiki.php.net/rfc/constructor_promotion)

This RFC adds syntactic sugar to create value objects or data transfer objects. Instead of specifying class properties and a constructor for them, PHP can now combine them into one.

Instead of doing this:

```php
class Money
{
    public Currency $currency;

    public int $amount;

    public function __construct(
        Currency $currency,
        int $amount,
    ) {
        $this->currency = $currency;
        $this->amount = $amount;
    }
}
```

You can now do this:

```php
class Money
{
    public function __construct(
        public Currency $currency,
        public int $amount,
    ) {}
}
```

There's a lot more to tell about property promotion, you can read about them [in this dedicated post](/blog/constructor-promotion-in-php-8).

---

<br>

### New `static` return type [RFC](https://wiki.php.net/rfc/static_return_type)

While it was already possible to return `self`, `static` wasn't a valid return type until PHP 8. Given PHP's dynamically typed nature, it's a feature that will be useful to many developers.

```php
class Foo
{
    public function test(): static
    {
        return new static();
    }
}
```

---

<br>

### New `mixed` type [RFC](https://wiki.php.net/rfc/mixed_type_v2)

Some might call it a necessary evil: the `mixed` type causes many to have mixed feelings. There's a very good argument to make for it though: a missing type can mean lots of things in PHP:

- A function returns nothing or null
- We're expecting one of several types
- We're expecting a type that can't be type hinted in PHP

Because of the reasons above, it's a good thing the `mixed` type is added. `mixed` itself means one of these types:

- `array`
- `bool`
- `callable`
- `int`
- `float`
- `null`
- `object`
- `resource`
- `string`

Note that `mixed` can also be used as a parameter or property type, not just as a return type.

Also note that since `mixed` already includes `null`, it's not allowed to make it nullable. The following will trigger an error:

```php
// Fatal error: Mixed types cannot be nullable, null is already part of the mixed type.
function bar(): ?mixed {}
```

---

<br>

### Throw expression [RFC](https://wiki.php.net/rfc/throw_expression)

This RFC changes `throw` from being a statement to being an expression, which makes it possible to throw exception in many new places:

```php
$triggerError = fn () => throw new MyError();

$foo = $bar['offset'] ?? throw new OffsetDoesNotExist('offset');
```

---

<br>

### Inheritance with private methods [RFC](https://wiki.php.net/rfc/inheritance_private_methods)

Previously, PHP used to apply the same inheritance checks on public, protected and private methods. In other words: private methods should follow the same method signature rules as protected and public methods. This doesn't make sense, since private methods won't be accessible by child classes.

This RFC changed that behaviour, so that these inheritance checks are not performed on private methods anymore. Furthermore, the use of `final private function` also didn't make sense, so doing so will now trigger a warning:

```
Warning: Private methods cannot be final as they are never overridden by other classes
```

---

<br>

### Weak maps [RFC](https://wiki.php.net/rfc/weak_maps)

Built upon the [weakrefs RFC](https://wiki.php.net/rfc/weakrefs) that was added in PHP 7.4, a `WeakMap` implementation is added in PHP 8. `WeakMap` holds references to objects, which don't prevent those objects from being garbage collected.

Take the example of ORMs, they often implement caches which hold references to entity classes to improve the performance of relations between entities. These entity objects can not be garbage collected, as long as this cache has a reference to them, even if the cache is the _only_ thing referencing them.

If this caching layer uses weak references and maps instead, PHP will garbage collect these objects when nothing else references them anymore. Especially in the case of ORMs, which can manage several hundreds, if not thousands of entities within a request; weak maps can offer a better, more resource friendly way of dealing with these objects.

Here's what weak maps look like, an example from the RFC:

```php
class Foo
{
    private WeakMap $cache;

    public function getSomethingWithCaching(object $obj): object
    {
        return $this->cache[$obj]
           ??= $this->computeSomethingExpensive($obj);
    }
}
```

---

<br>

### Allowing `::class` on objects [RFC](https://wiki.php.net/rfc/class_name_literal_on_object)

A small, yet useful, new feature: it's now possible to use `::class` on objects, instead of having to use `get_class()` on them. It works the same way as `get_class()`.

```txt
$foo = new Foo();

var_dump($foo::class);
```

---

<br>

### Non-capturing catches [RFC](https://wiki.php.net/rfc/non-capturing_catches)

Whenever you wanted to catch an exception before PHP 8, you had to store it in a variable, regardless whether you used that variable or not. With non-capturing catches, you can omit the variable, so instead of this:

```php
try {
    // Something goes wrong
} catch (MySpecialException $exception) {
    Log::error("Something went wrong");
}
```

You can now do this:

```php
try {
    // Something goes wrong
} catch (MySpecialException) {
    Log::error("Something went wrong");
}
```

Note that it's required to always specify the type, you're not allowed to have an empty `catch`. If you want to catch all exceptions and errors, you can use `Throwable` as the catching type.

---

<br>

### Trailing comma in parameter lists [RFC](https://wiki.php.net/rfc/trailing_comma_in_parameter_list)

Already possible when calling a function, trailing comma support was still lacking in parameter lists. It's now allowed in PHP 8, meaning you can do the following:

```php
public function(
    string $parameterA,
    int $parameterB,
    Foo $objectfoo,
) {
    // …
}
```

As a sidenote: trailing commas are also supported in the `use` list of closures, this was an oversight and now added via a [separate RFC](https://wiki.php.net/rfc/trailing_comma_in_closure_use_list).

---

<br>

### Create `DateTime` objects from interface

You can already create a `DateTime` object from a `DateTimeImmutable` object using `DateTime::createFromImmutable($immutableDateTime)`, but the other way around was tricky. By adding `DateTime::createFromInterface()` and `DatetimeImmutable::createFromInterface()` there's now a generalised way to convert `DateTime` and `DateTimeImmutable` objects to each other.

```php
DateTime::createFromInterface(DateTimeInterface $other);

DateTimeImmutable::createFromInterface(DateTimeInterface $other);
```

---

<br>

### New `Stringable` interface [RFC](https://wiki.php.net/rfc/stringable)

The `Stringable` interface can be used to type hint anything that implements `__toString()`. Whenever a class implements `__toString()`, it automatically implements the interface behind the scenes and there's no need to manually implement it.

```php
class Foo
{
    public function __toString(): string
    {
        return 'foo';
    }
}

function bar(string|Stringable $stringable) { /* … */ }

bar(new Foo());
bar('abc');
```

---

<br>

### New `str_contains()` function [RFC](https://wiki.php.net/rfc/str_contains)

Some might say it's long overdue, but we finally don't have to rely on `strpos()` anymore to know whether a string contains another string.

Instead of doing this:

```php
if (strpos('string with lots of words', 'words') !== false) { /* … */ }
```

You can now do this

```php
if (str_contains('string with lots of words', 'words')) { /* … */ }
```

---

<br>

### New `str_starts_with()` and `str_ends_with()` functions [RFC](https://wiki.php.net/rfc/add_str_starts_with_and_ends_with_functions)

Two other ones long overdue, these two functions are now added in the core.

```php
str_starts_with('haystack', 'hay'); // true
str_ends_with('haystack', 'stack'); // true
```

---

<br>

### New `fdiv()` function [PR](https://github.com/php/php-src/pull/4769)

The new `fdiv()` function does something similar as the `fmod()` and `intdiv()` functions, which allows for division by 0. Instead of errors you'll get `INF`, `-INF` or `NAN`, depending on the case.

---

<br>

### New `get_debug_type()` function [RFC](https://wiki.php.net/rfc/get_debug_type)

`get_debug_type()` returns the type of a variable. Sounds like something `gettype()` would do? `get_debug_type()` returns more useful output for arrays, strings, anonymous classes and objects.

For example, calling `gettype()` on a class `\Foo\Bar` would return `object`. Using `get_debug_type()` will return the class name.

A full list of differences between `get_debug_type()` and `gettype()` can be found in the RFC.

---

<br>

### New `get_resource_id()` function [PR](https://github.com/php/php-src/pull/5427)

Resources are special variables in PHP, referring to external resources. One example is a MySQL connection, another one a file handle.

Each one of those resources gets assigned an ID, though previously the only way to know that id was to cast the resource to `int`:

```php
$resourceId = (int) $resource;
```

PHP 8 adds the `get_resource_id()` functions, making this operation more obvious and type-safe:

```php
$resourceId = get_resource_id($resource);
```

---

<br>

### Abstract methods in traits improvements [RFC](https://wiki.php.net/rfc/abstract_trait_method_validation)

Traits can specify abstract methods which must be implemented by the classes using them. There's a caveat though: before PHP 8 the signature of these method implementations weren't validated. The following was valid:

```php
trait Test {
    abstract public function test(int $input): int;
}

class UsesTrait
{
    use Test;

    public function test($input)
    {
        return $input;
    }
}
```

PHP 8 will perform proper method signature validation when using a trait and implementing its abstract methods. This means you'll need to write this instead:

```php
class UsesTrait
{
    use Test;

    public function test(int $input): int
    {
        return $input;
    }
}
```

---

<br>

### Object implementation of `token_get_all()` [RFC](https://wiki.php.net/rfc/token_as_object)

The `token_get_all()` function returns an array of values. This RFC adds a `PhpToken` class with a `PhpToken::tokenize()` method. This implementation works with objects instead of plain values. It consumes less memory and is easier to read.

---

<br>

### Variable syntax tweaks [RFC](https://wiki.php.net/rfc/variable_syntax_tweaks)

From the RFC: "the Uniform Variable Syntax RFC resolved a number of inconsistencies in PHP's variable syntax. This RFC intends to address a small handful of cases that were overlooked."

---

<br>

### Type annotations for internal functions [EXTERNALS](https://externals.io/message/106522)

Lots of people [pitched in](https://github.com/php/php-src/pulls?q=is%3Apr+label%3AStubs+is%3Aclosed) to add proper type annotations to all internal functions. This was a long standing issue, and finally solvable with all the changes made to PHP in previous versions. This means that internal functions and methods will have complete type information in reflection.

---

<br>

### `ext-json` always available [RFC](https://wiki.php.net/rfc/always_enable_json)

Previously it was possible to compile PHP without the JSON extension enabled, this is not possible anymore. Since JSON is so widely used, it's best developers can always rely on it being there, instead of having to ensure the extension exist first.

<br>

## Breaking changes

As mentioned before: this is a major update and thus there will be breaking changes. The best thing to do is take a look at the full list of breaking changes over at the [UPGRADING](https://github.com/php/php-src/blob/PHP-8.0/UPGRADING#L20) document.

Many of these breaking changes have been deprecated in previous 7.* versions though, so if you've been staying up-to-date over the years, it shouldn't be all that hard to upgrade to PHP 8.

<br>

### Consistent type errors [RFC](https://wiki.php.net/rfc/consistent_type_errors)

User-defined functions in PHP will already throw `TypeError`, but internal functions did not, they rather emitted warnings and returned `null`. As of PHP 8 the behaviour of internal functions have been made consistent.

---

<br>

### Reclassified engine warnings [RFC](https://wiki.php.net/rfc/engine_warnings)

Lots of errors that previously only triggered warnings or notices, have been converted to proper errors. The following warnings were changed.

- Undefined variable: warning instead of notice
- Undefined array index: warning instead of notice
- Division by zero: `DivisionByZeroError` exception instead of warning
- Attempt to increment/decrement property '%s' of non-object: `Error` exception instead of warning
- Attempt to modify property '%s' of non-object: `Error` exception instead of warning
- Attempt to assign property '%s' of non-object: `Error` exception instead of warning
- Creating default object from empty value: `Error` exception instead of warning
- Trying to get property '%s' of non-object: warning instead of notice
- Undefined property: %s::$%s: warning instead of notice
- Cannot add element to the array as the next element is already occupied: `Error` exception instead of warning
- Cannot unset offset in a non-array variable: `Error` exception instead of warning
- Cannot use a scalar value as an array: `Error` exception instead of warning
- Only arrays and `Traversables` can be unpacked: `TypeError` exception instead of warning
- Invalid argument supplied for foreach(): `TypeError` exception instead of warning
- Illegal offset type: `TypeError` exception instead of warning
- Illegal offset type in isset or empty: `TypeError` exception instead of warning
- Illegal offset type in unset: `TypeError` exception instead of warning
- Array to string conversion: warning instead of notice
- Resource ID#%d used as offset, casting to integer (%d): warning instead of notice
- String offset cast occurred: warning instead of notice
- Uninitialized string offset: %d: warning instead of notice
- Cannot assign an empty string to a string offset: `Error` exception instead of warning
- Supplied resource is not a valid stream resource: `TypeError` exception instead of warning

---

<br>

### The @ operator no longer silences fatal errors

It's possible that this change might reveal errors that again were hidden before PHP 8. Make sure to set `display_errors=Off` on your production servers!

---

<br>

### Default error reporting level

It's now `E_ALL` instead of everything but `E_NOTICE` and `E_DEPRECATED`. This means that many errors might pop up which were previously silently ignored, though probably already existent before PHP 8.

---

<br>

### Default PDO error mode [RFC](https://wiki.php.net/rfc/pdo_default_errmode)

From the RFC: _The current default error mode for PDO is silent. This means that when an SQL error occurs, no errors or warnings may be emitted and no exceptions thrown unless the developer implements their own explicit error handling._

This RFC changes the default error will change to `PDO::ERRMODE_EXCEPTION` in PHP 8.

---

<br>

### Concatenation precedence [RFC](https://wiki.php.net/rfc/concatenation_precedence)

While already deprecated in PHP 7.4, this change is now taken into effect. If you'd write something like this:

```php
echo "sum: " . $a + $b;
```

PHP would previously interpret it like this:

```php
echo ("sum: " . $a) + $b;
```

PHP 8 will make it so that it's interpreted like this:

```php
echo "sum: " . ($a + $b);
```

---

<br>

### Stricter type checks for arithmetic and bitwise operators [RFC](https://wiki.php.net/rfc/arithmetic_operator_type_checks)

Before PHP 8, it was possible to apply arithmetic or bitwise operators on arrays, resources or objects. This isn't possible anymore, and will throw a `TypeError`:

```php
[] % [42];
$object + 4;
```

---

<br>

### Namespaced names being a single token [RFC](https://wiki.php.net/rfc/namespaced_names_as_token)

PHP used to interpret each part of a namespace (separated by a backslash `\`) as a sequence of tokens. This RFC changed that behaviour, meaning reserved names can now be used in namespaces.

---

<br>

### Saner numeric strings [RFC](https://wiki.php.net/rfc/saner-numeric-strings)

PHP's type system tries to do a lot of smart things when it encounters numbers in strings. This RFC makes that behaviour more consistent and clear.

---

<br>

### Saner string to number comparisons [RFC](https://wiki.php.net/rfc/string_to_number_comparison)

This RFC fixes the very strange case in PHP where `0 == "foo"` results in `true`. There are some other edge cases like that one, and this RFC fixes them.

---

<br>

### Reflection changes

A few reflection methods have been deprecated:

- `ReflectionFunction::isDisabled()`
- `ReflectionParameter::getClass()`
- `ReflectionParameter::isCallable()`

You should now use `ReflectionType` to get information about a parameter's type:

```php
$reflectionParameter->getType()->allowsNull();
```

If the type is a single type, `ReflectionParameter::getType()` returns an instance of `ReflectionNamedType`, which you can get its name from and whether it's built-in:

```php
$reflectionParameter->getType()->getName();
$reflectionParameter->getType()->isBuiltin();
```

If the type is a union type however, you'll get an instance of `ReflectionUnionType`, which can give you an array of `ReflectionNamedType` like so:

```php
$reflectionParameter->getType()->getTypes();
```

Checking whether a type is a union or not can be done with an `instanceof` check:

```php
if ($reflectionParameter->getType() instanceof ReflectionNamedType) {
    // It's a single type
}

if ($reflectionParameter->getType() instanceof ReflectionUnionType) {
    // It's a union type
}
```

Next up, three method signatures of reflection classes have been changed:

```php
ReflectionClass::newInstance($args);
ReflectionFunction::invoke($args);
ReflectionMethod::invoke($object, $args);
```

Have now become:

```php
ReflectionClass::newInstance(...$args);
ReflectionFunction::invoke(...$args);
ReflectionMethod::invoke($object, ...$args);
```

The upgrading guide specifies that if you extend these classes, and still want to support both PHP 7 and PHP 8, the following signatures are allowed:

```php
ReflectionClass::newInstance($arg = null, ...$args);
ReflectionFunction::invoke($arg = null, ...$args);
ReflectionMethod::invoke($object, $arg = null, ...$args);
```

---

---

<br>

### Stable sorting [RFC](https://wiki.php.net/rfc/stable_sorting)

Before PHP 8, sorting algorithms were unstable. This means that the order of equal elements wasn't guaranteed. PHP 8 changes the behaviour of all sorting functions to stable sorting.

---

<br>

### Fatal error for incompatible method signatures [RFC](https://wiki.php.net/rfc/lsp_errors)

From the RFC: _Inheritance errors due to incompatible method signatures currently either throw a fatal error or a warning depending on the cause of the error and the inheritance hierarchy._

---

<br>

### Other deprecations and changes

During the PHP 7.* development, several deprecations were added that are now finalised in PHP 8.

- Deprecations in [PHP 7.2](https://wiki.php.net/rfc/deprecations_php_7_2)
- Deprecations in [PHP 7.3](https://wiki.php.net/rfc/deprecations_php_7_3)
- Deprecations in [PHP 7.4](https://wiki.php.net/rfc/deprecations_php_7_4)
- Locale-independent [float to string cast](https://wiki.php.net/rfc/locale_independent_float_to_string)

---

{.include-end https://raw.githubusercontent.com/brendt/stitcher.io/master/src/content/blog/2020-01-16-new-in-php-8.md}
