---
title: Admin guide
description: Wiki.js platform admin guide.
published: true
date: 2021-12-19T02:12:11.624Z
tags: category - wiki-js, page type - content, section - platforms, topic - admin-guide
editor: markdown
dateCreated: 2021-12-03T13:36:07.471Z
---

# Official admin guide

> :book: For a complete guide, please refer to [official guide](https://docs.requarks.io/). There you will find how to manage admin features.
{.is-info}

It includes, amongst other additional documentation:

- [Authentication *Manage users authentication.*](https://docs.requarks.io/auth)
- [Search engines *Discover available internal search engines and configure their behaviour.*](https://docs.requarks.io/search)
- [Storage *Content backup and synchronization.*](https://docs.requarks.io/storage)
- [Users, groups & permissions *How to manage access to content.*](https://docs.requarks.io/groups)
{.links-list}

Take a look [now](https://docs.requarks.io/)!
