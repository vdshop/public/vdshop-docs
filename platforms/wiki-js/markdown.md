---
title: Markdown
description: Wiki.js Markdown capabilities.
published: true
date: 2021-12-19T02:12:11.639Z
tags: category - wiki-js, page type - content, section - platforms, topic - markdown
editor: markdown
dateCreated: 2021-12-03T10:25:25.795Z
---

# Overview

> This page can be considered a continuation of [Markdown language](/en/documentation/software/languages/markdown) page. Make sure you have read it before reading on.
{.is-info}

<br>

## Markdown features support

Wiki.js supports the full [CommonMark specification](https://spec.commonmark.org/) + some useful extensions (including the *Github Flavored Markdown* addons).

<br>

## Cheat sheets

> :thumbsup: While editing a page, at the bottom of left sidebar, locate and click <kbd>?</kbd> icon for quick and useful hints:
>
> ![ui-markdown-formatting-help.png](/assets/images/platforms/wiki-js/markdown/ui-markdown-formatting-help.png =300x){.elevation-3 .radius-5}
{.is-success}

:bulb: Check also [Markdown generic cheat sheets](/en/documentation/software/languages/markdown#cheat-sheets).

<br>

## Quick styling

> Classes can be added to almost everything by appending *`{.class-name-1 .class-name-2 ...}`* to it.
>
> Defined properties apply to full paragraph they are appended to.
{.is-info}

<br>

### Quick styling tabs {.tabset}

<br>

#### Grid list

> This styling is specific to Wiki.js and will fall back to standard list styling in other applications.
{.is-warning}

`{.grid-list}`

```
- Item 1
- Item 2
{.grid-list}
```

The rendered output looks like this:

- Item 1
- Item 2
{.grid-list}

<br>

#### Links list

> This styling is specific to Wiki.js and will fall back to standard list styling in other applications.
{.is-warning}

> This formatting may not work properly for pure anchored links: `[Link](#anchor)`
{.is-warning}

`{.links-list}`

```
- [Link 1 *Extended link 1 description*](https://wiki.vdshop.es/en/home)
- [Link 2 *Extended link 2 description*](/en/home)
{.links-list}
```

The rendered output looks like this:

- [Link 1 *Extended link 1 description*](https://wiki.vdshop.es/en/home)
- [Link 2 *Extended link 2 description*](/en/home)
{.links-list}

<br>

#### Smaller text

> This styling is specific to Wiki.js and will fall back to standard list styling in other applications.
{.is-warning}

`{.body-2}`

```
This is normal text.

This is smaller text.{.body-2}
```

The rendered output looks like this:

This is normal text.

This is smaller text.{.body-2}

---

```
It can be applied also to:
- Grid lists
- Links lists
- Others
{.grid-list .body-2}
```

The rendered output looks like this:

It can be applied also to:
- Grid lists
- Links lists
- Others
{.grid-list .body-2}

<br>

#### Tabs

> This styling is specific to Wiki.js and will fall back to standard list styling in other applications.
{.is-warning}

`{.tabset}`

```

# Tabs {.tabset}

<br>

## First tab title

Any content here will go into the first tab...

<br>

## Second tab title

Any content here will go into the second tab...

<br>

## Third tab title

Any content here will go into the third tab...
```

Refer to [Tabs section](#tabs) for full usage reference.

<br>

#### Image elevation, radius and dimensions

> `.elevation-{n}` and `.radius-{n}` stylings are specific to Wiki.js and will fall back to standard list styling in other applications.
{.is-warning}

`{.elevation-3}`
`{.radius-5}`
`60x`

```
![magento-logo-demo.jpeg](/assets/images/platforms/wiki-js/markdown/magento-logo-demo.jpeg =60x){.elevation-3 .radius-5}
```

The rendered output looks like this:

![magento-logo-demo.jpeg](/assets/images/platforms/wiki-js/markdown/magento-logo-demo.jpeg =60x){.elevation-3 .radius-5}

---

`{.elevation-5}`
`{.radius-3}`
`30x`

```
![magento-logo-demo.jpeg](/assets/images/platforms/wiki-js/markdown/magento-logo-demo.jpeg =30x){.elevation-5 .radius-3}
```

The rendered output looks like this:

![magento-logo-demo.jpeg](/assets/images/platforms/wiki-js/markdown/magento-logo-demo.jpeg =30x){.elevation-5 .radius-3}

<br>

#### Blockquotes

> These stylings are specific to Wiki.js and will fall back to standard list styling in other applications.
{.is-warning}

```
> This is a default unstyled blockquote.

> This is a `{.is-info}` blockquote.
{.is-info}

> This is a `{.is-success}` blockquote.
{.is-success}

> This is a `{.is-warning}` blockquote.
{.is-warning}

> This is a `{.is-danger}` blockquote.
{.is-danger}
```

The rendered output looks like this:

> This is a default unstyled blockquote.

> This is a `{.is-info}` blockquote.
{.is-info}

> This is a `{.is-success}` blockquote.
{.is-success}

> This is a `{.is-warning}` blockquote.
{.is-warning}

> This is a `{.is-danger}` blockquote.
{.is-danger}

<br>

#### Collapsible text

```html
<details>
<summary>Click here to see collapsed text</summary>
Hello, here is collapsed text!
</details>
```

The rendered output looks like this:

<details>
<summary>Click here to see collapsed text</summary>
Hello, here is collapsed text!
</details>

# Blockquotes

<br>

### Usage

Using a <kbd>></kbd> symbol, followed by a space, before each line of text.

<br>

### Shortcuts

By selecting text, then clicking the ![ui-markdown-blockquotes.png](/assets/images/platforms/wiki-js/markdown/ui-markdown-blockquotes.png =25x){.elevation-3 .radius-2} button in the toolbar.

<br>

### Examples

```
> Lorem ipsum dolor sit amet
> Consectetur adipiscing elit
```

The rendered output looks like this:

> Lorem ipsum dolor sit amet
> Consectetur adipiscing elit

<br>

### Stylings

Define a custom style class in a dedicated line.

Refer to [Quick styling](#quick-styling) section, `Blockquotes` tab, for available options.

# **Bold**

<br>

### Usage

Wrap text with <kbd>**</kbd>.

<br>

### Shortcuts

By selecting text, then clicking the ![Bold](https://docs.requarks.io/assets/ui/ui-markdown-bold.png =x25){.elevation-3 .radius-2} button in the toolbar.

By selecting text, then pressing <kbd>CMD</kbd> + <kbd>B</kbd>

<br>

### Examples

```
Lorem **ipsum** dolor
```

The rendered output looks like this:

Lorem **ipsum** dolor

# Code Blocks

<br>

### Usage

Wrap text with <kbd>```</kbd> on dedicated lines.

<br>

### Examples

````

```js
function lorem (ipsum) {
  const dolor = 'consectetur adipiscing elit'
}
```

````

The rendered output looks like this:

```js
function lorem (ipsum) {
  const dolor = 'consectetur adipiscing elit'
}
```

<br>

## Syntax Highlighting

By default, a code block is rendered as plain preformatted text. It's however preferable to use syntax highlighting for programming code, allowing for easier readability. To specify the programming language used in the code block, simply add the language keyword right after the opening triple backticks:

````java

```java
// some code here
```

````

Refer to the [reference list](https://github.com/highlightjs/highlight.js/blob/main/SUPPORTED_LANGUAGES.md) of about 185 supported programming languages.

# Collapsible text

In order to keep a page readable, you can use collapsible sections to hide large portions of text.

You can achieve it like this:

```html
<details>
<summary>Click here to see collapsed text</summary>
Hello, here is collapsed text!
</details>
```

The rendered output looks like this:

<details>
<summary>Click here to see collapsed text</summary>
Hello, here is collapsed text!
</details>

# Diagrams

<br>

### Usage

- Using **[Mermaid](#mermaid)** (click link for more advanced usage and examples)

- Using the **Diagram built-in editor** tool in the left toolbar:

![ui-diagrams.png](/assets/images/platforms/wiki-js/markdown/ui-diagrams.png =200x){.elevation-3 .radius-4}

<br>

### Examples

- Using **[Mermaid](#mermaid)** (click link for more advanced usage and examples):

````

```mermaid
graph LR
  START([Start]) --> SUBPROCESS[[Subprocess]] --> END([End])
```

````

The rendered output looks like this:

```mermaid
graph LR
  START([Start]) --> SUBPROCESS[[Subprocess]] --> END([End])
```

- Using the **built-in diagram editor** tool in the left toolbar:

```diagram
PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB2ZXJzaW9uPSIxLjEiIHdpZHRoPSIzMzVweCIgaGVpZ2h0PSI2MXB4IiB2aWV3Qm94PSItMC41IC0wLjUgMzM1IDYxIiBjb250ZW50PSImbHQ7bXhmaWxlIGhvc3Q9JnF1b3Q7ZW1iZWQuZGlhZ3JhbXMubmV0JnF1b3Q7IG1vZGlmaWVkPSZxdW90OzIwMjEtMDUtMTBUMTI6MDc6MDEuMTAxWiZxdW90OyBhZ2VudD0mcXVvdDs1LjAgKE1hY2ludG9zaDsgSW50ZWwgTWFjIE9TIFggMTFfMl8yKSBBcHBsZVdlYktpdC81MzcuMzYgKEtIVE1MLCBsaWtlIEdlY2tvKSBDaHJvbWUvODkuMC40Mzg5LjExNCBTYWZhcmkvNTM3LjM2JnF1b3Q7IHZlcnNpb249JnF1b3Q7MTQuNi4xMSZxdW90OyBldGFnPSZxdW90OzVySG9SbmJyZWNqcUE5QXo2VWxIJnF1b3Q7IHR5cGU9JnF1b3Q7ZW1iZWQmcXVvdDsmZ3Q7Jmx0O2RpYWdyYW0gaWQ9JnF1b3Q7bG83TDV3LUhScDBhTDZEOTJCS0EmcXVvdDsgbmFtZT0mcXVvdDtQYWdlLTEmcXVvdDsmZ3Q7eFZaTmI5c3dEUDAxdmc2MlpidkpjVXV6OWpKc1FBN3JqcXJGMmNKa3k1RGwydG12SDEzSm4wcTd3Z2lRUzBBK2tSVDUrS0RZSTRlaWUxQzB5cjlKQnNJTGZkWjU1TjRMdzMyVTRHOFBuQTJRUkRzRFpJb3pBd1VUY09KL3dZSytSUnZPb0Y0RWFpbUY1dFVTVEdWWlFxb1hHRlZLdHN1dzMxSXNiNjFvQmc1d1NxbHcwWitjNmR5Z3UvQnV3aCtCWi9sd2M1RHN6VWxCaDJBN1NaMVRKdHNaUkk0ZU9TZ3B0YkdLN2dDaTUyN2d4ZVI5ZmVOMGJFeEJxVCtTRUpxRUZ5b2FPOXRKVTZWdGMvbzhUSXhaU0M0Nlg5cWNhemhWTk8xUFdsd3ZZcmt1QkhvQm1yWWVLQTNkbXowRjQ2U29FSkFGYUhYR0VKdXdzOXhZY1FURSt1MUVkV0toZk1aeWJERnFsNXVOaGFmNTBiQVVYS2FET0hRY1MzWlRNa2djM1l5TnlCVkg4MXdwbVVKZE82U2drcXZlSE03L1M4NHpUZjlrU2pZbCs5NW93VXU0SG1uQmZpV2hNSFpJQzhJTHJDVlhZQzEyV0hNRlZMTFAvVE9FWGlwb1hmTjBTUTEwWEQvTjdGOW8rNTlpNjkzM1UvcURjeDZjRXZ0OG1qdXpyTjZkMGw2OUljODBCOHg1OFZaazR3Q3lVU2tzM2cxOEt6TFFNN1c0SzVsUkhyK2pVd1dDYXY2eWJPTFNHdXdOUHlUSDlxYU5SOHVOajM4VlF3blR2TTJhUDRqclFtdnByQ1ZoUm5ZS3ZhcGlIUHREUXJtN2dsQTJMbjJMd0xZSkpYS0ZRbTRwRkJLczlodkgyNFFTK211aHJBcHRGZ3E2MHdlQUNaKytvc2p4SHc9PSZsdDsvZGlhZ3JhbSZndDsmbHQ7L214ZmlsZSZndDsiPjxkZWZzLz48Zz48ZWxsaXBzZSBjeD0iMzAiIGN5PSIzMCIgcng9IjMwIiByeT0iMjUiIGZpbGw9IiNmZmZmZmYiIHN0cm9rZT0iIzAwMDAwMCIgcG9pbnRlci1ldmVudHM9ImFsbCIvPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0wLjUgLTAuNSkiPjxzd2l0Y2g+PGZvcmVpZ25PYmplY3Qgc3R5bGU9Im92ZXJmbG93OiB2aXNpYmxlOyB0ZXh0LWFsaWduOiBsZWZ0OyIgcG9pbnRlci1ldmVudHM9Im5vbmUiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIHJlcXVpcmVkRmVhdHVyZXM9Imh0dHA6Ly93d3cudzMub3JnL1RSL1NWRzExL2ZlYXR1cmUjRXh0ZW5zaWJpbGl0eSI+PGRpdiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94aHRtbCIgc3R5bGU9ImRpc3BsYXk6IGZsZXg7IGFsaWduLWl0ZW1zOiB1bnNhZmUgY2VudGVyOyBqdXN0aWZ5LWNvbnRlbnQ6IHVuc2FmZSBjZW50ZXI7IHdpZHRoOiA1OHB4OyBoZWlnaHQ6IDFweDsgcGFkZGluZy10b3A6IDMwcHg7IG1hcmdpbi1sZWZ0OiAxcHg7Ij48ZGl2IHN0eWxlPSJib3gtc2l6aW5nOiBib3JkZXItYm94OyBmb250LXNpemU6IDA7IHRleHQtYWxpZ246IGNlbnRlcjsgIj48ZGl2IHN0eWxlPSJkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IGZvbnQtc2l6ZTogMTJweDsgZm9udC1mYW1pbHk6IEhlbHZldGljYTsgY29sb3I6ICMwMDAwMDA7IGxpbmUtaGVpZ2h0OiAxLjI7IHBvaW50ZXItZXZlbnRzOiBhbGw7IHdoaXRlLXNwYWNlOiBub3JtYWw7IHdvcmQtd3JhcDogbm9ybWFsOyAiPlN0YXJ0PC9kaXY+PC9kaXY+PC9kaXY+PC9mb3JlaWduT2JqZWN0Pjx0ZXh0IHg9IjMwIiB5PSIzNCIgZmlsbD0iIzAwMDAwMCIgZm9udC1mYW1pbHk9IkhlbHZldGljYSIgZm9udC1zaXplPSIxMnB4IiB0ZXh0LWFuY2hvcj0ibWlkZGxlIj5TdGFydDwvdGV4dD48L3N3aXRjaD48L2c+PGVsbGlwc2UgY3g9IjMwNCIgY3k9IjMwIiByeD0iMzAiIHJ5PSIyNSIgZmlsbD0iI2ZmZmZmZiIgc3Ryb2tlPSIjMDAwMDAwIiBwb2ludGVyLWV2ZW50cz0iYWxsIi8+PGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTAuNSAtMC41KSI+PHN3aXRjaD48Zm9yZWlnbk9iamVjdCBzdHlsZT0ib3ZlcmZsb3c6IHZpc2libGU7IHRleHQtYWxpZ246IGxlZnQ7IiBwb2ludGVyLWV2ZW50cz0ibm9uZSIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgcmVxdWlyZWRGZWF0dXJlcz0iaHR0cDovL3d3dy53My5vcmcvVFIvU1ZHMTEvZmVhdHVyZSNFeHRlbnNpYmlsaXR5Ij48ZGl2IHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hodG1sIiBzdHlsZT0iZGlzcGxheTogZmxleDsgYWxpZ24taXRlbXM6IHVuc2FmZSBjZW50ZXI7IGp1c3RpZnktY29udGVudDogdW5zYWZlIGNlbnRlcjsgd2lkdGg6IDU4cHg7IGhlaWdodDogMXB4OyBwYWRkaW5nLXRvcDogMzBweDsgbWFyZ2luLWxlZnQ6IDI3NXB4OyI+PGRpdiBzdHlsZT0iYm94LXNpemluZzogYm9yZGVyLWJveDsgZm9udC1zaXplOiAwOyB0ZXh0LWFsaWduOiBjZW50ZXI7ICI+PGRpdiBzdHlsZT0iZGlzcGxheTogaW5saW5lLWJsb2NrOyBmb250LXNpemU6IDEycHg7IGZvbnQtZmFtaWx5OiBIZWx2ZXRpY2E7IGNvbG9yOiAjMDAwMDAwOyBsaW5lLWhlaWdodDogMS4yOyBwb2ludGVyLWV2ZW50czogYWxsOyB3aGl0ZS1zcGFjZTogbm9ybWFsOyB3b3JkLXdyYXA6IG5vcm1hbDsgIj5FbmQ8L2Rpdj48L2Rpdj48L2Rpdj48L2ZvcmVpZ25PYmplY3Q+PHRleHQgeD0iMzA0IiB5PSIzNCIgZmlsbD0iIzAwMDAwMCIgZm9udC1mYW1pbHk9IkhlbHZldGljYSIgZm9udC1zaXplPSIxMnB4IiB0ZXh0LWFuY2hvcj0ibWlkZGxlIj5FbmQ8L3RleHQ+PC9zd2l0Y2g+PC9nPjxyZWN0IHg9IjExMCIgeT0iMCIgd2lkdGg9IjEyMCIgaGVpZ2h0PSI2MCIgZmlsbD0iI2ZmZmZmZiIgc3Ryb2tlPSIjMDAwMDAwIiBwb2ludGVyLWV2ZW50cz0iYWxsIi8+PHBhdGggZD0iTSAxMjIgMCBMIDEyMiA2MCBNIDIxOCAwIEwgMjE4IDYwIiBmaWxsPSJub25lIiBzdHJva2U9IiMwMDAwMDAiIHN0cm9rZS1taXRlcmxpbWl0PSIxMCIgcG9pbnRlci1ldmVudHM9ImFsbCIvPjxnIHRyYW5zZm9ybT0idHJhbnNsYXRlKC0wLjUgLTAuNSkiPjxzd2l0Y2g+PGZvcmVpZ25PYmplY3Qgc3R5bGU9Im92ZXJmbG93OiB2aXNpYmxlOyB0ZXh0LWFsaWduOiBsZWZ0OyIgcG9pbnRlci1ldmVudHM9Im5vbmUiIHdpZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIHJlcXVpcmVkRmVhdHVyZXM9Imh0dHA6Ly93d3cudzMub3JnL1RSL1NWRzExL2ZlYXR1cmUjRXh0ZW5zaWJpbGl0eSI+PGRpdiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94aHRtbCIgc3R5bGU9ImRpc3BsYXk6IGZsZXg7IGFsaWduLWl0ZW1zOiB1bnNhZmUgY2VudGVyOyBqdXN0aWZ5LWNvbnRlbnQ6IHVuc2FmZSBjZW50ZXI7IHdpZHRoOiA5NHB4OyBoZWlnaHQ6IDFweDsgcGFkZGluZy10b3A6IDMwcHg7IG1hcmdpbi1sZWZ0OiAxMjNweDsiPjxkaXYgc3R5bGU9ImJveC1zaXppbmc6IGJvcmRlci1ib3g7IGZvbnQtc2l6ZTogMDsgdGV4dC1hbGlnbjogY2VudGVyOyAiPjxkaXYgc3R5bGU9ImRpc3BsYXk6IGlubGluZS1ibG9jazsgZm9udC1zaXplOiAxMnB4OyBmb250LWZhbWlseTogSGVsdmV0aWNhOyBjb2xvcjogIzAwMDAwMDsgbGluZS1oZWlnaHQ6IDEuMjsgcG9pbnRlci1ldmVudHM6IGFsbDsgd2hpdGUtc3BhY2U6IG5vcm1hbDsgd29yZC13cmFwOiBub3JtYWw7ICI+U3VicHJvY2VzczwvZGl2PjwvZGl2PjwvZGl2PjwvZm9yZWlnbk9iamVjdD48dGV4dCB4PSIxNzAiIHk9IjM0IiBmaWxsPSIjMDAwMDAwIiBmb250LWZhbWlseT0iSGVsdmV0aWNhIiBmb250LXNpemU9IjEycHgiIHRleHQtYW5jaG9yPSJtaWRkbGUiPlN1YnByb2Nlc3M8L3RleHQ+PC9zd2l0Y2g+PC9nPjxwYXRoIGQ9Ik0gNjAgMzAgTCAxMDMuNjMgMzAiIGZpbGw9Im5vbmUiIHN0cm9rZT0iIzAwMDAwMCIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBwb2ludGVyLWV2ZW50cz0ic3Ryb2tlIi8+PHBhdGggZD0iTSAxMDguODggMzAgTCAxMDEuODggMzMuNSBMIDEwMy42MyAzMCBMIDEwMS44OCAyNi41IFoiIGZpbGw9IiMwMDAwMDAiIHN0cm9rZT0iIzAwMDAwMCIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBwb2ludGVyLWV2ZW50cz0iYWxsIi8+PHBhdGggZD0iTSAyMzAgMzAgTCAyNjcuNjMgMzAiIGZpbGw9Im5vbmUiIHN0cm9rZT0iIzAwMDAwMCIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBwb2ludGVyLWV2ZW50cz0ic3Ryb2tlIi8+PHBhdGggZD0iTSAyNzIuODggMzAgTCAyNjUuODggMzMuNSBMIDI2Ny42MyAzMCBMIDI2NS44OCAyNi41IFoiIGZpbGw9IiMwMDAwMDAiIHN0cm9rZT0iIzAwMDAwMCIgc3Ryb2tlLW1pdGVybGltaXQ9IjEwIiBwb2ludGVyLWV2ZW50cz0iYWxsIi8+PC9nPjxzd2l0Y2g+PGcgcmVxdWlyZWRGZWF0dXJlcz0iaHR0cDovL3d3dy53My5vcmcvVFIvU1ZHMTEvZmVhdHVyZSNFeHRlbnNpYmlsaXR5Ii8+PGEgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMCwtNSkiIHhsaW5rOmhyZWY9Imh0dHBzOi8vd3d3LmRpYWdyYW1zLm5ldC9kb2MvZmFxL3N2Zy1leHBvcnQtdGV4dC1wcm9ibGVtcyIgdGFyZ2V0PSJfYmxhbmsiPjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIGZvbnQtc2l6ZT0iMTBweCIgeD0iNTAlIiB5PSIxMDAlIj5WaWV3ZXIgZG9lcyBub3Qgc3VwcG9ydCBmdWxsIFNWRyAxLjE8L3RleHQ+PC9hPjwvc3dpdGNoPjwvc3ZnPg==
```

<br>

## Mermaid

- **[Overview and tutorials](#mermaid-overview-and-tutorials)**
- **[Usage](#mermaid-usage)**
- **[Sequence diagram example](#mermaid-sequence-diagram-example)**
- **[Flowchart example](#mermaid-flowchart-example)**
{.grid-list}

<br>

### Overview and tutorials {#mermaid-overview-and-tutorials}

Mermaid is a **text-based diagramming tool** that lets you create diagrams and visualizations using text and code.

**The main purpose of Mermaid is to help with Visualizing Documentation, and helping it catch up with Development.**

> Documentation-Rot is a [Catch-22](https://en.wikipedia.org/wiki/Catch-22_(logic)) that Mermaid helps to solve.

Diagramming and documentation costs precious developer time and gets outdated quickly. But not having diagrams or docs ruins productivity and hurts organizational learning.

Mermaid addresses this [Catch-22](https://en.wikipedia.org/wiki/Catch-22_(logic)) by **cutting the time, effort and tooling that is required to create modifiable diagrams and charts, for smarter and more reusable content**. Mermaid, as a text-based diagramming tool allows for quick and easy updates, it can also be made part of production scripts (and other pieces of code), to make documentation much easier.

> Mermaid is a Diagramming tool for everyone.

Even non-programmers can create diagrams through the [Mermaid Live Editor](https://github.com/mermaid-js/mermaid-live-editor). Visit the [Tutorials Page](https://mermaid-js.github.io/mermaid/#/./Tutorials) for the Live Editor video tutorials, or get easily started [here](https://mermaid-js.github.io/mermaid/#/./n00b-gettingStarted).

<br>

### Usage {#mermaid-usage}

Using a [Code block](#code-blocks) with the language `mermaid`.

Refer to [Mermaid website](https://mermaid-js.github.io/mermaid) for a complete language reference.

<br>

### Sequence diagram example {#mermaid-sequence-diagram-example}

````

```mermaid
sequenceDiagram
    Alice ->> Bob: Hello Bob, how are you?
    Bob-->>John: How about you John?
    Bob--x Alice: I am good thanks!
    Bob-x John: I am good thanks!
    Note right of John: Bob thinks a long time.

    Bob-->Alice: Checking with John...
    Alice->John: Yes... John, how are you?
```

````

The rendered output looks like this:

```mermaid
sequenceDiagram
    Alice ->> Bob: Hello Bob, how are you?
    Bob-->>John: How about you John?
    Bob--x Alice: I am good thanks!
    Bob-x John: I am good thanks!
    Note right of John: Bob thinks a long time.

    Bob-->Alice: Checking with John...
    Alice->John: Yes... John, how are you?
```

<br>

### Flowchart example {#mermaid-flowchart-example}

````

```mermaid
graph LR
  %% Optimistic flow
  STATUS_NEW([New])
    --> SUB_INTERNAL_REVIEW[[Internal review]]
    -- Gather information Initial prioritization --> STATUS_WAITING_STRATEGIC_APPROVAL([Waiting strategic approval])
    -- Review --> SUB_ENGINEERING_MANAGER_REVIEW[[Engineering manager review]]
    -- Analysis prioritization --> STATUS_READY_FOR_ANALYSIS([Ready for analysis])
    --> SUB_WORKING_GROUP_DRI_ASSIGNATION[[Working group and DRI assignation]]
    --> CONNECTOR_PHASE_1((Phase 1))

  %% Optimistic flow: branches
  SUB_INTERNAL_REVIEW
    -.. Missing info ..-> STATUS_PROPOSAL_FEEDBACK_REQUIRED([Proposal feedback required])
    -.. Review info ..-> SUB_INTERNAL_REVIEW

  SUB_ENGINEERING_MANAGER_REVIEW -- Not fits strategically --> STATUS_REJECTED([Rejected])
  SUB_ENGINEERING_MANAGER_REVIEW -.. Additional info required ..-> STATUS_PROPOSAL_FEEDBACK_REQUIRED
```

````

The rendered output looks like this:

```mermaid
graph LR
  %% Optimistic flow
  STATUS_NEW([New])
    --> SUB_INTERNAL_REVIEW[[Internal review]]
    -- Gather information Initial prioritization --> STATUS_WAITING_STRATEGIC_APPROVAL([Waiting strategic approval])
    -- Review --> SUB_ENGINEERING_MANAGER_REVIEW[[Engineering manager review]]
    -- Analysis prioritization --> STATUS_READY_FOR_ANALYSIS([Ready for analysis])
    --> SUB_WORKING_GROUP_DRI_ASSIGNATION[[Working group and DRI assignation]]
    --> CONNECTOR_PHASE_1((Phase 1))

  %% Optimistic flow: branches
  SUB_INTERNAL_REVIEW
    -.. Missing info ..-> STATUS_PROPOSAL_FEEDBACK_REQUIRED([Proposal feedback required])
    -.. Review info ..-> SUB_INTERNAL_REVIEW

  SUB_ENGINEERING_MANAGER_REVIEW -- Not fits strategically --> STATUS_REJECTED([Rejected])
  SUB_ENGINEERING_MANAGER_REVIEW -.. Additional info required ..-> STATUS_PROPOSAL_FEEDBACK_REQUIRED
```

# Emojis

<br>

### Usage

Using the syntax `:identifier:`

<br>

## Cheat sheet

> See the **[Emoji Cheat Sheet](https://www.webfx.com/tools/emoji-cheat-sheet/)** for the full list of possible options.
{.is-info}

<br>

### Frequently used

- :heavy_check_mark: `:heavy_check_mark:`
- :white_check_mark: `:white_check_mark:`
- :information_source: `:information_source:`
- :x: `:x:`
- :no_entry_sign: `:no_entry_sign:`
- :thumbsup: `:thumbsup:`
- :thumbsdown: `:thumbsdown:`
- :ok_hand: `:ok_hand:`
- :smile: `:smile:`
- :bulb: `:bulb:`
- :arrow_right: `:arrow_right:`
- :book: `:book:`
- :fire: `:fire:`
- :wrench: `:wrench:`
- :mag: `:mag:`
- :lock: `:lock:`
- :unlock: `:unlock:`
{.grid-list}

<br>

### Examples

```
:apple:

Can be also be used :fire: inline
```

The rendered output looks like this:

:apple:

Can also be used :fire: inline.

# Footnotes

<br>

### Usage

Use the syntax `[^1]` for the location of the footnote in the main text, and `[^1]: this is a footnote` for the actual footnote. Footnotes themselves will automatically appear at the bottom of the page under a horizontal line.

<br>

### Examples

```
This sentence[^1] needs a few footnotes.[^2]

[^1]: A string of syntactic words.
[^2]: A useful example sentence.
```

The rendered output looks like this:

This sentence[^1] needs a few footnotes.[^2]

[^1]: A string of syntactic words.
[^2]: A useful example sentence.

# Headers

<br>

### Usage

Using between 1 and 6 <kbd>#</kbd> symbol(s), followed by a space, before the text selection.

> Headers with levels 1 and 2 will be automatically used for building *Table of Contents*.
{.is-info}

<br>

### Shortcuts

On the desired line, then clicking the ![ui-markdown-headers.png](/assets/images/platforms/wiki-js/markdown/ui-markdown-headers.png =x25){.elevation-3 .radius-2} dropdown button in the toolbar.

On the desired line, press <kbd>CMD</kbd> + <kbd>ALT</kbd> + <kbd>Right</kbd> to increase the header level.
On the desired line, press <kbd>CMD</kbd> + <kbd>ALT</kbd> + <kbd>Left</kbd> to decrease the header level.

> When working with Google Chrome (maybe with other browsers too), keyboard shortcuts may not work, as those keys combinations are already assigned for switching between browser tabs.
{.is-warning}

<br>

### Examples

```

<br>

#### H4: Why doing this?

This is very important. Find below reasons for doing it.

<br>

##### H5: Reason 1

This reason is the first one.

<br>

##### H5: Reason 2

This reason is the second one.

<br>

###### H6: Aclaration about reason 2

Reason 2 is important because this and that.
```

The rendered output looks like this:

<br>

#### H4: Why doing this?

This is very important. Find below reasons for doing it.

<br>

##### H5: Reason 1

This reason is the first one.

<br>

##### H5: Reason 2

This reason is the second one.

<br>

###### H6: Aclaration about reason 2

Reason 2 is important because this and that.

<br>

## Custom header IDs

Adding custom ids for headers is [possible](https://www.markdownguide.org/extended-syntax/#heading-ids) and *partially* supported.

This is mainly useful when having different headers with the same name under different sections. Defining an explicit id has some advantages, like allowing to change the heading text without modifying the anchor id, so links to it won't break.

> **Please be aware that TOC menu does not work properly regarding headers with custom IDs [(click here for details)](https://github.com/Requarks/wiki/discussions/4740).**
> **Use this feature only when really required.**
{.is-warning}

You can define a specific id for a header:

```

<br>

### My Great Heading {#custom-id}
```

So you can refer to it as:

```
Go to [this specific content](my-page#custom-id)
```

# Horizontal line

<br>

### Usage

Writing <kbd>---</kbd> on a dedicated line.

<br>

### Shortcuts

By clicking the ![Horizontal Rule](https://docs.requarks.io/assets/ui/ui-markdown-hr.png =x25){.elevation-3 .radius-2} button in the toolbar.

<br>

### Examples

```
Lorem ipsum dolor

---

Consectetur adipiscing elit
```

The rendered output looks like this:

Lorem ipsum dolor

---

Consectetur adipiscing elit

# Image

<br>

### Usage

Using the syntax `![Image Caption](Image Source)`.

<br>

### Shortcuts

Using the **Assets** tool in the left toolbar:

![ui-assets.png](/assets/images/platforms/wiki-js/markdown/ui-assets.png =200x){.elevation-3 .radius-4}

<br>

### Examples

```
![Lorem ipsum](https://dolor.example.com/sit/amet.jpg)

Consectetur ![adipiscing](/link/to/image.jpg) elit
```

<br>

### Stylings

Sometimes images are too large or maybe you want the image to fill up all the available space.

Simply add the dimensions at the end of the image path in the following format:

```
![Image](/link/to/image.jpg =100x50)
```

You can also omit one of the values to automatically keep the image ratio:

```
![Image](/link/to/image.jpg =100x)
![Image](/link/to/image.jpg =x50)
```

It's also possible to use other units, like %. Useful when you need the image to take all the available space:

```
![Image](/link/to/image.jpg =100%x)
```

Refer to [Image elevation, radius and dimensions](#quick-styling) for further styling info.

# Inline code

<br>

### Usage

Wrap text with single <kbd>`</kbd> symbol.

<br>

### Shortcuts

By selecting text, then clicking the ![ui-inline-code.png](/assets/images/platforms/wiki-js/markdown/ui-inline-code.png =x25){.elevation-3 .radius-2} button in the toolbar.

<br>

### Examples

```
Lorem `ipsum` dolor
```

The rendered output looks like this:

Lorem `ipsum` dolor

# *Italic*

<br>

### Usage

Wrap text with single <kbd>*</kbd> symbol.

<br>

### Shortcuts

By selecting text, then clicking the ![Italic](https://docs.requarks.io/assets/ui/ui-markdown-italic.png =x25){.elevation-3 .radius-2} button in the toolbar.

By selecting text, then pressing <kbd>CMD</kbd> + <kbd>I</kbd>

<br>

### Examples

```
Lorem *ipsum* dolor
```

The rendered output looks like this:

Lorem *ipsum* dolor

# <kbd>Keyboard key</kbd>

<br>

### Usage

Using `<kbd>` before and `</kbd>` after the text selection.

<br>

### Shortcuts

By selecting text, then clicking the ![ui-keyboard-key.png](/assets/images/platforms/wiki-js/markdown/ui-keyboard-key.png =x25){.elevation-3 .radius-2} button in the toolbar.

<br>

### Examples

```
Lorem ipsum dolor <kbd>CMD</kbd> + <kbd>C</kbd>
```

The rendered output looks like this:

Lorem ipsum dolor <kbd>CMD</kbd> + <kbd>C</kbd>

# [Link](#link)

<br>

### Usage

Using the syntax `[Link text](Link target)`. Append `#section` to `Link target` navigate to a specific section of destination page.

<br>

### Shortcuts

Using the **Link** tool in the left toolbar:

![ui-links.png](/assets/images/platforms/wiki-js/markdown/ui-links.png =200x){.elevation-3 .radius-4}

> Link tool works only for internal pages. Use default syntax for external links.
{.is-warning}

<br>

### Examples

```
[Lorem ipsum](https://wiki.js.org/about)

Consectetur [adipiscing](/en/home#section) elit
```

The rendered output looks like this:

[Lorem ipsum](https://wiki.js.org/about)

Consectetur [adipiscing](/en/home#section) elit

# Lists: checklist

<br>

### Usage

Using either `- [ ]` or `- [x]`.

<br>

### Examples

```
- [x] Checked task item
- [x] Another checked task item
- [ ] Unchecked task item
```

The rendered output looks like this:

- [x] Checked task item
- [x] Another checked task item
- [ ] Unchecked task item

# Lists: ordered

<br>

### Usage

Using a **number**, followed by a <kbd>.</kbd> symbol and a space, before each line of text.

> :thumbsup: Using **`1.`** for all list items will render them in increasing order, and will make easier to reorder items without having to renumber them all.
{.is-success}

<br>

### Shortcuts

By selecting text, then clicking the ![ui-markdown-ol.png](https://docs.requarks.io/assets/ui/ui-markdown-ol.png =x25){.elevation-3 .radius-2} button in the toolbar.

<br>

### Examples

```
1. Lorem ipsum dolor sit amet
1. Consectetur adipiscing elit
1. Morbi vehicula aliquam
```

The rendered output looks like this:

1. Lorem ipsum dolor sit amet
1. Consectetur adipiscing elit
1. Morbi vehicula aliquam

# Lists: unordered

<br>

### Usage

Using an <kbd>*</kbd> or a <kbd>-</kbd> symbol, followed by a space, before each line of text.

<br>

### Shortcuts

By selecting text, then clicking the ![Blockquote](https://docs.requarks.io/assets/ui/ui-markdown-ul.png =x25){.elevation-3 .radius-2} button in the toolbar.

<br>

### Examples

```
- Lorem ipsum dolor sit amet
- Consectetur adipiscing elit
- Morbi vehicula aliquam
```

The rendered output looks like this:

- Lorem ipsum dolor sit amet
- Consectetur adipiscing elit
- Morbi vehicula aliquam

<br>

### Stylings

By adding a class on a separate line, after the list, you can change the look of the list:

- `links-list`
- `grid-list`

For example:

```
- Grid Item 1
- Grid Item 2
- Grid Item 3
{.grid-list}

This is a separation dummy text.

- [Link Title 1 *Subtitle description here*](https://www.google.com)
- [Link Title 2 *Another subtitle description here*](https://www.google.com)
- [Link Title 3 *Third subtitle description here*](https://www.google.com)
{.links-list}
```

The rendered output looks like this:

- Grid Item 1
- Grid Item 2
- Grid Item 3
{.grid-list}

This is a separation dummy text.

- [Link Title 1 *Subtitle description here*](https://www.google.com)
- [Link Title 2 *Another subtitle description here*](https://www.google.com)
- [Link Title 3 *Third subtitle description here*](https://www.google.com)
{.links-list}

# ~~Strikethrough~~

<br>

### Usage

Wrap text with <kbd>~~</kbd>.

<br>

### Shortcuts

By selecting text, then clicking the ![Strikethrough](https://docs.requarks.io/assets/ui/ui-markdown-strike.png =x25){.elevation-3 .radius-2} button in the toolbar.

<br>

### Examples

```
Lorem ~~ipsum~~ dolor
```

The rendered output looks like this:

Lorem ~~ipsum~~ dolor

# Sub~scripts~

<br>

### Usage

Wrap text with single <kbd>~</kbd> symbol.

<br>

### Shortcuts

By selecting text, then clicking the ![ui-subscript.png](/assets/images/platforms/wiki-js/markdown/ui-subscript.png =x25){.elevation-3 .radius-2} button in the toolbar.

<br>

### Examples

```
Lorem ~ipsum~ dolor
```

The rendered output looks like this:

Lorem ~ipsum~ dolor

# Super^scripts^

<br>

### Usage

Wrap text with single <kbd>^</kbd> symbol.

<br>

### Shortcuts

By selecting text, then clicking the ![ui-superscript.png](/assets/images/platforms/wiki-js/markdown/ui-superscript.png =x25){.elevation-3 .radius-2} button in the toolbar.

<br>

### Examples

```
Lorem ^ipsum^ dolor
```

The rendered output looks like this:

Lorem ^ipsum^ dolor

# Tables

<br>

### Usage

To add a table, use three or more hyphens <kbd>---</kbd> to create each column’s header, and use pipes <kbd>|</kbd> to separate each column. You can optionally add pipes on either end of the table.

You can format the text within tables. For example, you can add links, code (words or phrases in backticks <kbd>`</kbd> only, not code blocks), and emphasis.

> You can’t add headings, blockquotes, lists, horizontal rules, images, or HTML tags.
{.is-warning}

<br>

## Tables generator

> :bulb: **Tip:** Creating tables with hyphens and pipes can be tedious. To speed up the process, **use the [Markdown Tables Generator](https://www.tablesgenerator.com/markdown_tables)**.
>
> Build a table using the graphical interface, and then copy the generated Markdown-formatted text into your file. It is also able to load an existing table in Markdown format, so you can import table there, edit it and bring back the modified result.
{.is-success}

<br>

### Escaping Pipe Characters

You can display a pipe <kbd>|</kbd> character in a table by using its HTML character code (`&#124;`).

<br>

### Alignment

You can align text in the columns to the left, right, or center by adding a colon <kbd>:</kbd> to the left, right, or on both side of the hyphens within the header row.

```
| Syntax | Description | Test Text |
| :--- | :----: | ---: |
| Header | Title | Here's this |
| Paragraph | Text | And more |
```

The rendered output looks like this:

| Syntax | Description | Test Text |
| :--- | :----: | ---: |
| Header | Title | Here's this |
| Paragraph | Text | And more |

<br>

### Examples

```
| Syntax | Description |
| ----------- | ----------- |
| Header | Title |
| Paragraph | Text |
```

The rendered output looks like this:

| Syntax | Description |
| ----------- | ----------- |
| Header | Title |
| Paragraph | Text |

---

Cell widths can vary, as shown below. The rendered output will look the same.

```
| Syntax | Description |
| --- | ----------- |
| Header | Title |
| Paragraph | Text |
```

# Tabs

<br>

### Usage

Using headers and adding the `{.tabset}` class to the parent header. The parent header text will not be shown in the final result.

Note that you can use any header level, as long as the children headers are one level higher. For example, if a parent header is `###` *(h3)*, the tabs headers must be `###` *(h4)*. The maximum header level for a parent being 5 and the children 6.

<br>

### Examples

```

# Tabs {.tabset}

<br>

## First Tab

Any content here will go into the first tab...

<br>

## Second Tab

Any content here will go into the second tab...

<br>

## Third Tab

Any content here will go into the third tab...
```

# _Underline_

<br>

### Usage

Wrap text with single <kbd>_</kbd>.

<br>

### Examples

```
This is some _underlined_ text.
```

The rendered output looks like this:

This is some _underlined_ text.

# Video

<br>

### Usage

You can embed videos by pasting the code provided by video platform.

<br>

### Examples

```html
<iframe width="560" height="315" src="https://www.youtube.com/embed/N0a9DS3Jwlw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
```

The rendered output looks like this:

<iframe width="560" height="315" src="https://www.youtube.com/embed/N0a9DS3Jwlw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></iframe>
