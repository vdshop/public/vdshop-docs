---
title: User guide
description: Wiki.js platform user guide.
published: true
date: 2021-12-19T02:12:11.656Z
tags: category - wiki-js, page type - content, section - platforms, topic - user-guide
editor: markdown
dateCreated: 2021-12-03T13:36:14.675Z
---

# Official user guide

> :book: For a complete user guide, please refer to [official user guide](https://docs.requarks.io/#user-guide). There you will find how to manage basic and advanced features.
{.is-info}

It includes, amongst other additional documentation:

- [Folder Structure & Tags *Learn how to categorize your pages for an easier browsing experience.*](https://docs.requarks.io/guide/structure)
- [Manage Pages *How to create, edit and manage your pages.*](https://docs.requarks.io/guide/pages)
- [Using Editors *Learn how to use the various editors.*](https://docs.requarks.io/editors)
- [Using Assets *How to upload and manage assets such as images and documents.*](https://docs.requarks.io/guide/assets)
{.links-list}

Take a look [now](https://docs.requarks.io/#user-guide)!

> :thumbsup: Remember to read also [Markdown Wiki.js specific documentation: cheat sheets and user guide](/en/platforms/wiki-js/markdown).
{.is-success}

# Basics

We've brought here some basic concepts for convenience.

<br>

## :thumbsup: Thumbs-up rules

- :mag: Use **Search bar** or [**Tag search**](/t) to find content.
- :bookmark_tabs: Click **Main menu / Browse** button to toggle between `Main menu` and `Browse` menu modes.
- :eyeglasses: You may get a better visualization experience by adjusting your **browser zoom to 80%**, depending on your screen size.
- :unlock: **Use the same identity provider at login**, in order to always work with the same account.
- :wrench: **Check your profile to [update your preferences](/p/profile)**, like your timezone or theme settings.
{.grid-list}

<br>

## Interface overview

![Interface](https://docs.requarks.io/assets/ui/ui-basics.jpg){.decor-radius .decor-shadow}

<br>

### Global

- **Global Navigation** - A persistent navigation menu, displayed on all pages. Usually consists of important pages or links to external websites. Toggle *Browse* / *Main Menu* for different navigation types.
- **Global / Tag Search** - Quickly find a page by performing a search. Click Tag icon for Tag search.
- **Create New Page** - Create a new page.
- **User Menu** - User-specific actions such as View Profile, Administration (if available) and Logout.
{.grid-list}

<br>

### Per page

- **Page Actions** - List of actions on the current page, such as Edit, Move, Delete, History, etc. If you cannot see this icon, it means you cannot perform any action on current page.
- **Breadcrumbs** - Full Path to the current page. Represents the folder structure.
- **Table of Contents** - Sections of the current page. Based on the headers in the content.
- **Page Tags** - Tags associated to the current page. See related pages by clicking on a tag.
- **Author** - View the author and date of the last modification of the page.
- **Social Links** - Sharing / Printing Links for the current page.
- **Edit Page / Page Actions** - Quick access menu to edit or perform other actions on the current page. If you cannot see this icon, it means you cannot perform any action on current page.
{.grid-list}

<br>

## Create a new page

In order to create a new page, click the **New Page** button, located in the top right corner of the page.

The following dialog will appear:

![New page dialog](https://docs.requarks.io/assets/ui/ui-newpage-dialog.png =600x){.elevation-3 .radius-5}

1. Select the language to create the page for *(the current locale is selected by default)*.
2. Enter the full path to the page you want to create.

> The path [MUST](/en/handbook/wikishop#rfc-2119) contain only **URL-safe characters**: <kbd>a-z</kbd> <kbd>A-Z</kbd> <kbd>0-9</kbd> <kbd>_</kbd> <kbd>-</kbd>.
> The path [MUST NOT](/en/handbook/wikishop#rfc-2119) end with a **trailing slash** <kbd>/</kbd>.
{.is-danger}

:thumbsup: **You don't need to create folders**. Enter the full path you want to create and folders will be created automatically. For example, enter `universe/planets/earth` to automatically create the universe and planets subfolders.

3. Click **Select** to proceed.

<br>

## Select an editor

When creating a new page, you'll be prompted with the following dialog:

![Select Editor Dialog](https://docs.requarks.io/assets/ui/ui-selecteditor-dialog.png =600x){.elevation-3 .radius-5}

For a complete list of editors and how to use them, refer to the official documentation [Editors](https://docs.requarks.io/editors) section.

<br>

## Enter page metadata

Upon selecting an editor, you'll be prompted with the Page Metadata dialog:

![Page Metadata Dialog](https://docs.requarks.io/assets/ui/ui-pageprops-dialog.png =600x){.elevation-3 .radius-5}

Enter a **title**, **description** and **tags** for your page.

Finally, click **OK** to close the dialog and start writing!

> You can always return to the Page Metadata dialog later by clicking the **Page** button, located in the upper-right corner of the page.
{.is-info}

<br>

## Save

Your page is not created until you hit the **Create** button (located in the upper-right corner of the page). It will be saved and rendered, after which you'll automatically be redirected to the final result. Simply click the edit button to go back to editing!
