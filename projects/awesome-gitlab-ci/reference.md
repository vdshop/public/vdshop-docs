---
title: Reference
description: Reference directory index.
published: true
date: 2021-12-19T02:12:11.743Z
tags: category - awesome-gitlab-ci, page type - index, section - projects, topic - reference
editor: markdown
dateCreated: 2021-12-19T02:12:09.582Z
---

{.directory-index-start }

# Browse by content

- [Definitions *Definitions directory index.*](/en/projects/awesome-gitlab-ci/reference/definitions)
- [Templates](/en/projects/awesome-gitlab-ci/reference/templates)
{.links-list}

# Browse by tags

[Browse related content by tags](/t/category%20-%20awesome-gitlab-ci/category%20-%20reference/page%20type%20-%20content/section%20-%20projects/?sort=title).

{.directory-index-end }
