---
title: Definitions
description: Definitions directory index.
published: true
date: 2021-12-19T02:12:11.789Z
tags: category - awesome-gitlab-ci, category - reference, page type - index, section - projects, topic - definitions
editor: markdown
dateCreated: 2021-12-19T02:12:09.615Z
---

{.directory-index-start }

# Browse by content

- [Artifacts *Predefined set of artifacts definitions.*](/en/projects/awesome-gitlab-ci/reference/definitions/artifacts)
- [Scripts](/en/projects/awesome-gitlab-ci/reference/definitions/scripts)
{.links-list}

# Browse by tags

[Browse related content by tags](/t/category%20-%20awesome-gitlab-ci/category%20-%20reference/category%20-%20definitions/page%20type%20-%20content/section%20-%20projects/?sort=title).

{.directory-index-end }
