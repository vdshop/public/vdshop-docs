---
title: Artifacts
description: Predefined set of artifacts definitions.
published: true
date: 2021-12-20T13:52:40.615Z
tags: category - awesome-gitlab-ci, category - reference, category - definitions, page type - content, section - projects, topic - artifacts
editor: markdown
dateCreated: 2021-11-29T16:18:44.330Z
---

In `.gitlab-ci.yml` file, artifacts may be defined:
- at [`default` section](https://docs.gitlab.com/ee/ci/yaml/#default).
- within a [job](https://docs.gitlab.com/ee/ci/yaml/#artifacts).

[Read more about artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html).

# default

> **Default artifact definition.**
> Allows to set and use a generic artifact directory, suitable for most use cases.
{.is-success}

<br>

## Description

- Initializes `.build/artifacts` directory.
- Instructs GitLab to use that directory as an artifact, by defining also:
  - `default` as artifact name.
  - `public: false` as privacy policy.
  - `when: on_success` as push / creation policy.
- Files that have been put there during job execution are automatically available in next CI stages, or at jobs in the same stage using [`needs`](https://docs.gitlab.com/ee/ci/yaml/#needsartifacts) or [`dependencies`](https://docs.gitlab.com/ee/ci/yaml/#dependencies) keywords.

<br>

## Examples

Usage at `default` level:

```yaml
default:
  artifacts: !reference [ .definitions, artifacts, default, definition ]
  before_script:
    - !reference [ .definitions, artifacts, default, init ]

my-job:
  inherit:
    default: true
```

Usage at `job` level:

```yaml
my-job:
  artifacts: !reference [ .definitions, artifacts, default, definition ]
  before_script:
    - !reference [ .definitions, artifacts, default, init ]
```

<br>

## Advanced usage

- **definition:** Actual definition itself:
  - `!reference [ .definitions, artifacts, default, definition ]`
- **init**: Needed additional scripts to initialize the definition:
  - `!reference [ .definitions, artifacts, default, init ]`
- **template**: Preconfigured definition to be used via [`extends`](https://docs.gitlab.com/ee/ci/yaml/#extends).
  - See `.templates_artifacts_default` usage in examples above.

> Please note that:
> :warning: For direct definition usage, **init** needs be included under `before_script` or `script` keywords.
> :warning: Manual merge of array sections may be needed, as `extends` [merges hashes but not arrays](https://docs.gitlab.com/ee/ci/yaml/yaml_optimization.html#merge-details).
> See examples above.
{.is-warning}
