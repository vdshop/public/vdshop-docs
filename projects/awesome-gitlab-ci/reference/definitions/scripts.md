---
title: Scripts
description: 
published: true
date: 2021-12-19T02:12:11.860Z
tags: category - awesome-gitlab-ci, category - reference, category - definitions, page type - content, section - projects, topic - scripts
editor: markdown
dateCreated: 2021-11-30T12:16:36.277Z
---

# init_shell_options

<br>

### Description

Init shell options. May be defined under `before_script` or `script` keywords.

<br>

### Value

```yaml
value:
  - set -o errexit -o nounset
```

<br>

### Usage

Reference usage:

```yaml
!reference [ .agc, definitions, scripts, init_shell_options, value ]
```

Example usages:

```yaml
default:
  before_script:
    (...)
    - !reference [ .agc, definitions, scripts, init_shell_options, value ]
    (...)

my-job:
  inherit:
    default: true
```

```yaml
my-job:
  before_script:
    (...)
    - !reference [ .agc, definitions, scripts, init_shell_options, value ]
    (...)
```

```yaml
my-job:
  script:
    (...)
    - !reference [ .agc, definitions, scripts, init_shell_options, value ]
    (...)
```
