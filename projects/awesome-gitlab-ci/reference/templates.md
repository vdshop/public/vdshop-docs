---
title: Templates
description: 
published: true
date: 2021-12-19T02:12:11.823Z
tags: category - awesome-gitlab-ci, category - reference, page type - content, section - projects, topic - templates
editor: markdown
dateCreated: 2021-11-29T14:25:22.527Z
---

# Artifacts

Predefined set of artifacts templates.

<br>

## default

> Default artifact template. See [](/en/projects/awesome-gitlab-ci/reference/definitions#default) Allows to set and use a generic artifact directory, suitable for most use cases.
{.is-success}

<br>

### Description

- Initializes `.build/artifacts` directory.
- Instructs GitLab to use that directory as an artifact, by defining also:
  - `default` as artifact name.
  - `public: false` as privacy policy.
  - `when: on_success` as push / creation policy.
- Files that have been put there are automatically available in next CI stages, or at jobs in the same stage using [`needs`](https://docs.gitlab.com/ee/ci/yaml/#needsartifacts) or [`dependencies`](https://docs.gitlab.com/ee/ci/yaml/#dependencies) keywords. Learn more about artifacts [here](https://docs.gitlab.com/ee/ci/yaml/#artifacts) and [here](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html).

<br>

### Usage

- **definition:** Actual definition itself:
  - `!reference [ .definitions, artifacts, default, definition ]`.
- **init**: Needed additional scripts to initialize the definition:
  - `!reference [ .definitions, artifacts, default, init ]`.
- **template**: Preconfigured definition to be used via [`extends`](https://docs.gitlab.com/ee/ci/yaml/#extends).
  - See `.templates_artifacts_default` usage in examples below.

> :warning: When extending more than one template, manual merge of additional array sections may be needed, as `extends` [merges hashes but not arrays](https://docs.gitlab.com/ee/ci/yaml/yaml_optimization.html#merge-details).
{.is-warning}

<br>

### Examples

At `default` level:

```yaml
default:
  extends:
    - .templates_artifacts_default

my-job:
  inherit:
    default: true
```

At `job` level:

```yaml
my-job:
  extends:
    - .templates_artifacts_default
```
