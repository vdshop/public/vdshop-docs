---
title: Overview
description: Docker CI Overview
published: true
date: 2021-12-20T11:23:10.700Z
tags: category - docker-ci, page type - content, section - projects, topic - overview
editor: markdown
dateCreated: 2021-12-12T01:23:06.203Z
---

![pipeline status](https://gitlab.com/vdshop/public/docker-images/docker/badges/main/pipeline.svg)
![Docker](https://img.shields.io/badge/Docker-latest-9cf?logo=docker)
![buildx](https://img.shields.io/badge/buildx-latest-brightgreen?logo=docker)
![compose](https://img.shields.io/badge/compose-latest-brightgreen?logo=docker)
![pushrm](https://img.shields.io/badge/pushrm-latest-brightgreen?logo=docker)
[![Docker Pulls](https://img.shields.io/docker/pulls/vdshop/docker-ci.svg?logo=docker)](https://hub.docker.com/r/vdshop/docker-ci/)

# Docker CI: cli + buildx + compose + pushrm

**This documentation is better visualized at [GitLab](https://gitlab.com/vdshop/docker-images/docker-ci/-/blob/main/README.md).**

This image:
  - can be used as a **transparent, drop-in replacement of [official Docker images](https://hub.docker.com/_/docker)**.
  - is meant to be used for CI / development.
  - provides some additional functionalities and preinstalled Docker plugins over base Docker images.

**By default, if not further configured, it'll behave the same as an official Docker image**, as this image is built on top of them.
**No action or configuration is taken unless configured**. See below for further details.

<br>

## Features summary

Available features:
  - **Inherited**: All features already provided by [official Docker images](https://hub.docker.com/_/docker).
  - **Automatic login to registry**: Allow login to registry when container starts, by using predefined environment variables.
  - **Multiplatform builds**: Allow multiplatform builds by preconfiguring `buildx` and [QEMU](https://www.qemu.org/) emulators when container starts, by using predefined environment variables.
  - **Compose**: Preinstalled, use either `docker compose` (v2) or `docker-compose` (v1), both work thanks to [compose-switch](https://github.com/docker/compose-switch) project.
  - **Push README to container registry**: Allow pushing / updating registry description and short description. Credits to [Christian Korneck](https://github.com/christian-korneck/docker-pushrm).
  - **Additional utilities**: Some additional scripts and utilities can be used:
    - `get-github-download-url`: Get GitHub download url for repo and release, based on current platform (os/arch).
    - `get-github-latest-release-tag`: Get GitHub latest release tag for repo.
    - `get-github-latest-tag`: Get GitHub latest tag for repo.
    - `install-from-github-release`: Install from GitHub release.

Coming soon:
- **Lint Dockerfile**: Built-in support for linting Dockerfiles using [`hadolint`](https://github.com/hadolint/hadolint).

<br>

### Used plugins

These Docker plugins are preinstalled and ready to be used:
- **buildx**: [Docker Buildx (Docker Inc.)](https://github.com/docker/buildx). See `Multiplatform builds` section for details and usage.
- **compose**: [Docker Compose (Docker Inc.)](https://github.com/docker/compose). See `Compose` section for details and usage.
- **pushrm**: [Push README to container registry (Christian Korneck)](https://github.com/christian-korneck/docker-pushrm). See `Push README to container registry` section for details and usage.

# Table of contents

[TOC]

# Tags, versions and supported platforms

<br>

## Tags

<br>

### Software versioned tags

This image tags are based on the software versions they contain for `cli`, `buildx`, `compose`, `switch`, `pushrm`.

> Please note that here `switch` refers to [compose-switch](https://github.com/docker/compose-switch).
> Also, `cli` refers to Docker (cli) version.

Software versioned tags follow the following format: `-buildx--compose--switch--pushrm-`

<br>

### `latest` tag

In addition to software versioned tags, a `latest` tag is available, built in a regular basis, containing the latest versions of all the software it includes.
This often results in `latest` tag and a software versioned tag with latest versions to be the same.
However, this way, for the sake of stability, you can stick to a set of software versions permanently, instead of relying in latest changes.

<br>

## Software versions

You can check actual software versions by running the following command:

```shell
docker pull vdshop/docker-ci
docker inspect vdshop/docker-ci --format=''
```

<br>

## Supported platforms

This image has been built for the following platforms:

- `linux/amd64`
- `linux/arm64`

> If additional platforms are desired to be supported, file a new feature request [here](https://gitlab.com/vdshop/public/docker-images/docker-ci/-/issues).
> Specify as issue title: `Feature request: additional platforms.`.
> Explain motivation for the feature request.

# Features details

<br>

## Automatic login to registry

This image allows login automatically to registry when container starts, by using predefined environment variables:

| Variable name | Optional | Default value | Description | Unset after used |
|-----------------------------|:-------------------------------------------------:|:-------------:|--------------------|:----------------:|
| DOCKER_CI_REGISTRY | yes | `docker.io` | Registry name. | no |
| DOCKER_CI_REGISTRY_USER | Yes, required only if automatic login is intended | none | Registry user. | yes |
| DOCKER_CI_REGISTRY_PASSWORD | Yes, required only if automatic login is intended | none | Registry password. | yes |

> If not configured, automatic login will be skipped.

This is mainly useful for CI or docker-compose based installations. You can still use any variables specified at [Docker base image](https://hub.docker.com/_/docker), including authentication specific ones.

Of course, you can still authenticate to registry via other methods, like bind-mounting or generating `~/.docker/config.json` file for authentication, or using [GitLab CI `DOCKER_AUTH_CONFIG`](https://docs.gitlab.com/runner/configuration/advanced-configuration.html#use-a-private-container-registry).
However, at some specific situations:
  - you may not be able to mount a `~/.docker/config.json`.
  - messing with `~/.docker/config.json` generation just for login is not worth the effort.
  - also, messing with `DOCKER_AUTH_CONFIG` json-encoded string with base64-encoded credentials just for login seems not worth the effort either.

<br>

## Multiplatform builds

This image allows multiplatform builds by preconfiguring [`buildx`](https://docs.docker.com/buildx/working-with-buildx/) and [QEMU](https://www.qemu.org/) emulators when container starts, by using predefined environment variables:

| Variable name | Optional | Default value | Description | Unset after used |
|:----------------------------------------:|:--------:|:-------------:|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:----------------:|
| DOCKER_CI_BUILDX_ENABLE | yes | `0` | Enable and configure `buildx` capabilities: - A `buildx` builder, named `multi-arch-builder`, is (re)created and configured to be used. - Docker is configured to use `buildx` behind the scenes when running `docker build`. - if `DOCKER_CI_BUILDX_CONFIGURE_MULTIPLATFORM` is set to `1` (which is by default), QEMU emulators for specified platforms (`all` by default) will be installed and configured. See details below. Please note that: - value needs to be set to `1` to enable this feature. Any other value won't work (`true`, `yes`, ...). - Docker daemon needs to be accessible to enable and configure `buildx`. | no |
| DOCKER_CI_BUILDX_CONFIGURE_MULTIPLATFORM | yes | `1` | Install and configure QEMU emulators for specified platforms. Please note that: - this feature is enabled by default, but only triggers when `DOCKER_CI_BUILDX_ENABLE` is set to `1`. - value can be set to other than `1` to disable (`0` recommended). - see `DOCKER_CI_BUILDX_PLATFORM_EMULATORS` to specify QEMU emulators to be installed. See details below. | no |
| DOCKER_CI_BUILDX_PLATFORM_EMULATORS | yes | `all` | QEMU emulators to be installed and configured. Please note that: - this variable will only be used when `DOCKER_CI_BUILDX_ENABLE` and `DOCKER_CI_BUILDX_CONFIGURE_MULTIPLATFORM` are set to `1`. - by default, all available emulators will be installed and configured. - You can limit emulators by specifying a list of comma-separated platforms (e.g. `linux/amd64,linux/arm64`). - Find further info [here](https://github.com/tonistiigi/binfmt#installing-emulators). | no |

Sources:
- [buildx](https://github.com/docker/buildx)
- [binfmt](https://github.com/tonistiigi/binfmt)

<br>

### Example usages: local

You may want to copy, adapt and export this snippet in your `.bashrc`, `.zshrc` or analog shell config file.

```shell
export DOCKER_CI_REGISTRY='docker.io'
export DOCKER_CI_REGISTRY_USER='user'
export DOCKER_CI_REGISTRY_PASSWORD='password'
export DOCKER_CI_BUILDX_ENABLE='1'
export DOCKER_CI_BUILDX_CONFIGURE_MULTIPLATFORM='1'
export DOCKER_CI_BUILDX_PLATFORM_EMULATORS='linux/amd64,linux/arm64'
export DOCKERFILE_PATH='Dockerfile'
export DOCKER_BUILD_CONTEXT_PATH='context'

docker-build-push() {
  local docker_tag

  docker_tag="${1:-latest}"

  docker run --rm -it \
    -e DOCKER_CI_REGISTRY \
    -e DOCKER_CI_REGISTRY_USER \
    -e DOCKER_CI_REGISTRY_PASSWORD \
    -e DOCKER_CI_BUILDX_ENABLE \
    -e DOCKER_CI_BUILDX_CONFIGURE_MULTIPLATFORM \
    -e DOCKER_CI_BUILDX_PLATFORM_EMULATORS \
    -w '/build' \
    -v "$(pwd):/build" \
    -v '/var/run/docker.sock:/var/run/docker.sock' \
    -- \
    vdshop/docker-ci build \
      --pull --push \
      --platform "${DOCKER_CI_BUILDX_PLATFORM_EMULATORS}" \
      --tag "${DOCKER_CI_REGISTRY}/${DOCKER_CI_REGISTRY_USER}/$(basename "$(pwd)"):${docker_tag}" \
      --file "${DOCKERFILE_PATH}" \
      -- \
      "${DOCKER_BUILD_CONTEXT_PATH}"
}
```

Then, run `source ~/.bashrc`, `source ~/.zshrc` or open a new terminal tab / window, `cd` into desired directory and run `docker-build-push` or `docker-build-push 'my-image-tag'`.

<br>

### Example usages: GitLab CI

This is a simple example of how it could be used in GitLab CI `.gitlab-ci.yml` file.

```yaml
'docker-build-push':
  image: 'vdshop/docker'
  stage: 'build'
  interruptible: true
  services:
    - 'docker:dind'
  cache:
    key: '${CI_JOB_NAME}' # Share cache for this job across branches and pipeline executions.
    policy: 'pull-push'
    paths:
      - '.cache/docker'
    when: 'always'
  variables:
    DOCKER_HOST: 'tcp://docker:2375/'
    DOCKER_DRIVER: 'overlay2'
    DOCKER_TLS_CERTDIR: '' # Skip TLS for this ephemeral dind instance.
    DOCKER_CI_REGISTRY: '${CI_REGISTRY}'
    DOCKER_CI_REGISTRY_USER: '${CI_REGISTRY_USER}'
    DOCKER_CI_REGISTRY_PASSWORD: '${CI_REGISTRY_PASSWORD}'
    DOCKER_CI_BUILDX_ENABLE: '1'
    DOCKER_CI_BUILDX_CONFIGURE_MULTIPLATFORM: '1'
    DOCKER_CI_BUILDX_PLATFORM_EMULATORS: '${CI_BUILD_PLATFORMS-linux/amd64,linux/arm64}'
    DOCKERFILE_PATH: 'Dockerfile'
    DOCKER_BUILD_CONTEXT_PATH: 'context'
    DOCKER_REGISTRY_REPO: '${CI_PROJECT_NAME}'
    DOCKER_TAG: ''
    DOCKER_BUILD_COMMAND_ADDITIONAL_ARGS: ''
  script:
    - 'set -eu'
    - '[ -n "${DOCKER_TAG-}" ] || { >&2 echo "[!] ERROR: DOCKER_TAG is not defined or empty, it must be explicitly defined." && exit 1; }'
    - 'mkdir -p .cache/docker'
    - 'docker build
        --pull
        --push
        --cache-from type=local,src=.cache/docker
        --cache-to type=local,dest=.cache/docker
        --platform "${DOCKER_CI_BUILDX_PLATFORM_EMULATORS}"
        --tag "${DOCKER_REGISTRY}/${DOCKER_REGISTRY_USER}/${DOCKER_REGISTRY_REPO}:${DOCKER_TAG}"
        --file "${DOCKERFILE_PATH}"
        ${DOCKER_BUILD_COMMAND_ADDITIONAL_ARGS}
        "${DOCKER_BUILD_CONTEXT_PATH}"'
  rules:
    - exists:
        - 'Dockerfile'
      changes:
        - 'Dockerfile'
        - '.dockerignore'
        - 'context/**/*'
```

Please note that `CI_REGISTRY`, `DOCKER_CI_REGISTRY_USER`, `CI_REGISTRY_PASSWORD`:
- Are automatically available and populated when the GitLab Container Registry is enabled for the project ([see docs](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)).
- Otherwise, for example when using DockerHub, they must be previously defined for the pipeline, either:
  - at `Settings` => `CI/CD` => `Variables` for project or any of its parent groups.
  - manually, when running pipeline manually or defining schedules at `CI/CD` => `Schedules`.

Also, don't forget to check if `CI_BUILD_PLATFORMS` default value fits your needs. If not, redefine it as for the variables explained above.

<br>

## Compose

Preinstalled, use either `docker compose` (v2) or `docker-compose` (v1), both work thanks to [compose-switch](https://github.com/docker/compose-switch) project.

<br>

## Push README to container registry

This image allows pushing / updating registry description and short description. It can use environment variables or command arguments.

> It requires being logged to registry to push descriptions. See `Automatic login to registry` section.

For its usage with variables:

| Variable name | Optional | Default value | Description | Unset after used |
|:-------------:|:--------:|:----------------------------------------------------------------------------------:|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|:----------------:|
| PUSHRM_FILE | yes | `README-containers.md`, `README.md` (if any of them exists, in specific order) | Define file which content will be pushed to registry as image full description. | no |
| PUSHRM_SHORT | yes | none | Define short description (string) that will be pushed to registry as short description. Please note that: - if variable is not defined, remote short description won't be modified. - if variable is defined, **even if empty**, remote short description will be modified. | no |
| PUSHRM_TARGET | no | none | Image full name for which the description will be updated. (e.g.: `docker.io/vdshop/docker-cli`) | no |

For its usage with command arguments, check out `pushrm` command help:

```shell
docker run --rm -it vdshop/docker-ci pushrm --help
```

<br>

### Example usages: local

You may want to copy, adapt and export this snippet in your `.bashrc`, `.zshrc` or analog shell config file.

```shell
export DOCKER_CI_REGISTRY='docker.io'
export DOCKER_CI_REGISTRY_USER='user'
export DOCKER_CI_REGISTRY_PASSWORD='password'

docker-pushrm() {
  set -x
  local short_description short_description_args

  short_description="${1-}"
  short_description_args=()

  [ -z "${short_description}" ] || short_description_args=("--short" "${short_description}")

  docker run --rm -it \
    -e DOCKER_CI_REGISTRY \
    -e DOCKER_CI_REGISTRY_USER \
    -e DOCKER_CI_REGISTRY_PASSWORD \
    -w '/build' \
    -v "$(pwd):/build" \
    -- \
    vdshop/docker-ci pushrm "${short_description_args[@]}" -- "${DOCKER_CI_REGISTRY}/${DOCKER_CI_REGISTRY_USER}/$(basename "$(pwd)")"
}
```

Then, run `source ~/.bashrc`, `source ~/.zshrc` or open a new terminal tab / window, `cd` into desired directory and run `docker-pushrm` or `docker-pushrm 'My short description.'`.

<br>

### Example usages: GitLab CI

```yaml
docker-pushrm:
  image: 'vdshop/docker'
  stage: 'deploy'
  interruptible: true
  variables:
    DOCKER_CI_REGISTRY: '${CI_REGISTRY}'
    DOCKER_CI_REGISTRY_USER: '${CI_REGISTRY_USER}'
    DOCKER_CI_REGISTRY_PASSWORD: '${CI_REGISTRY_PASSWORD}'
    PUSHRM_FILE: 'README.md'
    PUSHRM_SHORT: '${CI_PROJECT_TITLE}: ${CI_REGISTRY_SHORT_DESCRIPTION}'
    PUSHRM_TARGET: '${CI_REGISTRY}/${CI_REGISTRY_USER}/${CI_PROJECT_NAME}'
  script:
    - 'docker pushrm'
  rules:
    - exists:
        - 'README.md'
      changes:
        - 'README.md'
```

Please note that `CI_REGISTRY`, `DOCKER_CI_REGISTRY_USER`, `CI_REGISTRY_PASSWORD`:
- Are automatically available and populated when the GitLab Container Registry is enabled for the project ([see docs](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)).
- Otherwise, for example when using DockerHub, they must be previously defined for the pipeline, either:
  - at `Settings` => `CI/CD` => `Variables` for project or any of its parent groups.
  - manually, when running pipeline manually or defining schedules at `CI/CD` => `Schedules`.

Also, don't forget to set `CI_REGISTRY_SHORT_DESCRIPTION` for short description.

<br>

## Issues / feature requests

If needed, file new issues or feature requests [here](https://gitlab.com/vdshop/docker-images/docker-ci/-/issues).

<br>

## Authors

[vdSHOP Team](https://vdshop.es/)
