---
title: Known issues
description: GitLab CI known issues.
published: true
date: 2021-12-20T14:08:42.111Z
tags: category - gitlab-ci, page type - content, section - services, topic - known-issues
editor: markdown
dateCreated: 2021-11-30T19:42:45.926Z
---

> :x: **Issues** are bugs about the behaviour we could expect from the platform, regarding what the official documentation says.
{.is-info}

If referred to open an issue related with an *issue* in Gitlab, would lead to a Bug report.

*Issues* enforces us to take some **workarounds** to get things done.

See also [GitLab CI known limitations](/en/services/gitlab-ci/known-limitations).

# :x: Known issues

- [Referencing a `false` value *`!reference` keyword pointing to a `false` (boolean) value does not work.*](#referencing-a-false-value)
- [`extends` in global `variables` *Unable to use multiple extends, inconsistent behaviour and usage, unexpected YAML validation.*](#extends-in-global-variables)
- [`extends` and `!reference` together *`extends` does not work when refers to a hash that uses `!reference` for defining itself.*](#extends-and-reference-together)
{.links-list}

---

# :x: Issues

<br>

## :x: Referencing a *false* value

> It is **not possible** to use `!reference` keyword to **reference** a key containing a ***false* (boolean) value directly**.
> It is **not recommended** to use `!reference` keyword to **reference** a key containing a ***true* (boolean) value directly**.
{.is-info}

This example configuration does not work:

```yaml
.some_definition:
  interruptible: false

my-job:
  # Line below does not work at all, although it should.
  interruptible: !reference [ .some_definition, interruptible ]
  script: exit 0
```

> **This GitLab CI configuration is invalid: `!reference [".some_definition", "interruptible"]` could not be found.**
{.is-danger}

This example configuration works but it's **not recommended**, as it is **fragile** and **will break your pipeline** whenever referenced value changes to *false*:

```yaml
.some_definition:
  interruptible: true

my-job:
  # Line below works only as long as referenced value is `true`.
  interruptible: !reference [ .some_definition, interruptible ]
  script: exit 0
```

> **This GitLab CI configuration is valid.**
{.is-success}

<br>

### Workarounds / tips

- :thumbsup: **Avoid referencing keys with boolean values directly, even if they are currently defined as *true*.**
  - :warning: If referenced value changes its to *false*, it will break your pipeline.

Change approach to either:

- **use `extends`**.

```yaml
.default_settings:
  inherit:
    default: false
    variables: false

my-job:
  extends:
    - .default_settings
  script: exit 0
```

- **encapsulate configurations** containing boolean values into a **dedicated hash**, and **reference that hash instead**.

```yaml
.default_settings:
  inherit_settings:
    default: false
    variables: false

my-job:
  inherit: !reference [ .default_settings, inherit_settings ]
  script: exit 0
```

- Use boolean values directly.

```yaml
my-job:
  inherit:
    default: false
    variables: false
  script: exit 0
```

See explanation section for further details.

<br>

### Explanation

<details>
<summary>Explanation is collapsed. Click here to open/close it.</summary>

This is a simple configuration example which uses `interruptible` keyword deliberately, as it needs a boolean value (values like `0`, `'0'`, `'false'` are not valid values):

```yaml
.some_definition:
  interruptible: false

my-job:
  # Line below does not work !!
  interruptible: !reference [ .some_definition, interruptible ]
  script: exit 0
```

> **This GitLab CI configuration is invalid: `!reference [".some_definition", "interruptible"]` could not be found.**
{.is-danger}

Note that `!reference [ .some_definition, interruptible ]` points to a key with a *false* (boolean) value.

Simply changing referred value to true, it works:

```yaml
.some_definition:
  interruptible: true

my-job:
  # Line below now works.
  interruptible: !reference [ .some_definition, interruptible ]
  script: exit 0
```

> **This GitLab CI configuration is valid.**
{.is-success}

Somehow, keys with *false* boolean values behave as they are *null* and they get removed completely from the configuration. This makes impossible to assign an explicit shared `false` default value.

You may think that, for this concrete example, it does not make sense set `interruptible: false` as default shared value, as [`interruptible`](https://docs.gitlab.com/ee/ci/yaml/#interruptible) default value is already *false*. But it's not the case for all settings. Some of them have *true* as default value.

This is another example that uses [`inherit:default`](https://docs.gitlab.com/ee/ci/yaml/#inheritdefault) and [`inherit:variables`](https://docs.gitlab.com/ee/ci/yaml/#inheritvariables). Both of them default to `true`.

```yaml
.default_settings:
  inherit_default: false
  inherit_variables: false

my-job:
  inherit:
    default: !reference [ .default_settings, inherit_default ]
    variables: !reference [ .default_settings, inherit_variables ]
  script: exit 0
```

> **This GitLab CI configuration is invalid: !reference [".default_settings", "inherit_default"] could not be found.**
{.is-danger}

Inherit an explicit *false* value is not possible via direct `!reference`. If you still want to share boolean values, see workarounds and tips above.

</details>

<br>

## :x: Using `extends` in global `variables`

It is not possible to define global variables by aggregation using `extends`.

:information_source: To be fair, it actually works when extending only one template, but this limit defeats its aggregation purpose and goes against [GitLab docs example of using `extends` as an array](https://docs.gitlab.com/ee/ci/yaml/yaml_optimization.html#merge-details), which is actually possible in other contexts.

Reducing it to the simplest example possible, this should work:

```yaml
.variables_1:
  VAR_1: one

.variables_2:
  VAR_2: two

variables:
  extends:
    - .variables_1
    - .variables_2

my-job:
  script: exit 0
```

But this configuration results in the following error:

> **This GitLab CI configuration is invalid: variables config should be a hash of key value pairs, value can be a hash.**
{.is-danger}

<br>

### Workarounds / tips

- :thumbsup: **Change the `extends` approach, and use a combination of `include` and `variables` definition in included files to get them merged properly.**
- Examples provided in the [official documentation](https://docs.gitlab.com/ee/ci/yaml/yaml_optimization.html#use-extends-and-include-together) won't work in this scope.

<br>

#### Example

`.ci/vars-1.yml`:

```yaml
variables:
  VAR_1: one
```

`.ci/vars-2.yml`:

```yaml
variables:
  VAR_2: two
```

`.gitlab-ci.yml`:

```yaml
include:
  - local: .ci/vars-1.yml
  - local: .ci/vars-2.yml
```

Merged result:

```yaml
variables:
  VAR_1: one
  VAR_2: two
```

<br>

### Explanation

<details>
<summary>Explanation is collapsed. Click here to open/close it.</summary>

Well, this is a tricky one to explain in full, but let's try.

First of all, documentation is not very clear about usage of `extends` outside of a job:
- https://docs.gitlab.com/ee/ci/yaml/#variables does not specify whether it can use `extends` keyword or not.
- https://docs.gitlab.com/ee/ci/variables/ does not offer further information.
- https://docs.gitlab.com/ee/ci/yaml/yaml_optimization.html#use-extends-to-reuse-configuration-sections is not useful in this specific context.

The only place where we can find something more or less useful is at https://docs.gitlab.com/ee/ci/yaml/#extends, where it states the following regarding `extends` keyword:

> **Keyword type:** Job keyword. You can use it only as part of a job.

Looking at that statement, it seems that the behaviour we are analyzing should fall under *Limitations* instead of under *Issues*. `extends` usage is not explicitly allowed under `variables` section.

You may be asking yourself:

> Ok, so, why is this an *issue* instead of a *limitation*?

<br>

#### Reason 1: Inconsistent usage

If the previous definition is strictly true:
- We must not be able to use `extends` under global sections, like `default` and `variables`.
- We can expect a consistent behaviour when trying to use `extends` under those global sections.

When `extends` is used under `default` global keyword, it behaves as expected:

```yaml
.defaut_interruptible:
  interruptible: true

default:
  extends: .defaut_interruptible

my-job:
  script: exit 0
```

> **This GitLab CI configuration is invalid: default config contains unknown keys: extends.**
{.is-danger}

However, when `extends` is used under `variables` global keyword, it does not behave as expected, as this configuration is actually allowed:

```yaml
.variables_1:
  VAR_1: one

variables:
  extends: .variables_1

my-job:
  script: exit 0
```

> **This GitLab CI configuration is valid.**
{.is-success}

According to GitLab editor, produces this merged configuration:

```yaml
".variables_1":
  VAR_1: one
variables:
  VAR_1: one
  extends: ".variables_1"
my-job:
  script: exit 0
```

We can see in merged configuration that `extends` has been processed and `VAR_1: one` has been included as global variable. **According to documentation, this was not expected to be possible.**

<br>

#### Reason 2: Impossible to define a variable named `extends`

`variables` hash uses its keys as variable names. There is no way to difference whether `extends`, when put under `variables`, is an actual variable name or should be considered a keyword. Actual behaviour takes the second approach, what causes to be **impossible to define a variable named `extends`**:

```yaml
variables:
  extends: some_value

my-job:
  script: exit 0
```

> **This GitLab CI configuration is invalid: variables: unknown keys in `extends` (some_value).**
{.is-danger}

<br>

#### Reason 3: `extends` is validated as a variable, then evaluated as `extends`

We'll be assuming, from now on, that:
- The combo `variables -> extends` can be potentially used, since it exists at least one possible configuration that makes `extends` work as expected, so `extends` processing support must be there somewhere in an underlying layer, ready to be used.
- Keyword `extends` should be able to be used there as in any other section (string / array of strings).

Let's say we want to add another `extends` entry to the working example, so we modify it by changing `extends` to be an array of strings and adding our new entry. Given that `extends` can be used as an array of strings in another sections, we expect this configuration to work properly:

```yaml
.variables_1:
  VAR_1: one

.variables_2:
  VAR_2: two

variables:
  extends:
    - .variables_1
    - .variables_2

my-job:
  script: exit 0
```

But instead, it results in the following error:

> **This GitLab CI configuration is invalid: variables config should be a hash of key value pairs, value can be a hash.**
{.is-danger}

So, what's happening here? Global `variables` section accepts variables in two formats:

```yaml

# Simple format.

variables:
  VAR_1: one

# Extended format.

variables:
  VAR_1:
    value: one
    description: Variable one description.
```

The error above is about configuration validation, stating that expects `variables` formatted according to those formats. This validation occured also in the working example above, but it passed the validation because a simple extends, `extends: .variables_1`, matches variables simple declaration format, `variable_name: value`.

> In short, **`extends` is validated as a variable, then evaluated as `extends`.**
> That is what allows us to get a simple `extends` working, but does not allow it when declared as an array of templates.
{.is-info}

<br>

#### Conclusion

Fixing the validation for the `variables -> extends` case would allow to use it properly, at the reduced cost of having `extends` word reserved, so it couldn't be used as variable name. In this case, documentation should be updated to reflect actual behaviour. Pros and cons should be evaluated.

If achieving this is complicated or not desired, complete support for this case should be removed, so it is consistent with documentation and with other cases such as `default -> extends` explained above, which throws appropriate error.

</details>

<br>

## :x: `extends` and `!reference` together

> This case is ilustrated as *Alternative 1-4* at [Unsupported `extends` usage with nested keys alternatives](/en/services/gitlab-ci/known-limitations#extends-first-depth-level-only).
> Check sibling alternatives for available workarounds.
{.is-info}

<br>

### Description

`extends` does not work when refers to a hash that uses `!reference` for defining itself.

Problematic code:

```yaml
.definitions:
  .i_am_unreachable_with_extends:
    image: alpine:latest
    timeout: 20 minutes

.i_am_extendable: !reference [ .definitions, .i_am_unreachable_with_extends ]

my-job:
  extends:
    - .i_am_extendable
  script: exit 0
```

> **This GitLab CI configuration is invalid: my-job: invalid base hashes in `extends` (.i_am_extendable).**
{.is-danger}

Note how `.i_am_extendable` tries to define itself as a full copy of `.definitions -> .i_am_unreachable_with_extends`. Extending templates defined this way do not work.

<br>

### Explanation

Somehow, it seems that the processing of `!reference` and `extends` does not happen in the appropriate order. As a result, `.i_am_extendable` template has not been already populated with values coming from `!reference` when validation is performed.
