---
title: Known limitations
description: GitLab CI known limitations.
published: true
date: 2021-12-20T14:07:44.388Z
tags: category - gitlab-ci, page type - content, section - services, topic - known-limitations
editor: markdown
dateCreated: 2021-12-02T13:54:38.467Z
---

> :warning: **Limitations** are those things that work as documented and expected, but limit us to implement more advanced features.
{.is-info}

If referred to open an issue related with a *limitation* in Gitlab, would lead to a Feature request.

*Limitations* enforces us to take some **workarounds** to get things done.

See also [GitLab CI known issues](/en/services/gitlab-ci/known-issues).

# :warning: Known limitations

- [`extends`: first depth level only *Extending nested hashes is not supported.*](#extends-first-depth-level-only)
{.links-list}

---

# :warning: `extends`: first depth level only

> **`extends` keyword does not allow to reference a nested hash**.
> As a consequence, you cannot extend hashes that are not defined at first depth level as jobs or [hidden jobs / templates](https://docs.gitlab.com/ee/ci/jobs/index.html#hide-jobs).
{.is-info}

- **Problematic code:** unreachable configuration using `extends`:

```yaml
.definitions:
  .i_am_unreachable_with_extends:
    image: alpine:latest
    timeout: 20 minutes

my-job:
  extends:
    # Line below does not work.
    - .definitions.i_am_unreachable_with_extends
  script: exit 0
```

> **This GitLab CI configuration is invalid: my-job: unknown keys in `extends` (.definitions.i_am_unreachable_with_extends).**
{.is-danger}

<br>

## Workarounds / tips

- :thumbsup: **Flatten definitions / templates up to first depth level**, like [hidden jobs / templates](https://docs.gitlab.com/ee/ci/jobs/index.html#hide-jobs), to make them usable with `extends` keyword.
- :thumbsup: Alternatively, **`!reference` directly desired keys**.
  - :x: **[Avoid referencing keys with boolean values](/en/services/gitlab-ci/known-issues#referencing-a-false-value).**

<details>
  <summary><b>Click here to see available workaround alternatives.</b></summary>

<br>

### Alternative 1: Flatten definition up to first depth level

<br>

### Alternative 1 tabset {.tabset}

<br>

#### **Alternative 1-1**: define a template referencing its source values via `!reference`:

> :x: **[Avoid referencing keys with boolean values](/en/services/gitlab-ci/known-issues#referencing-a-false-value).**
{.is-warning}

```yaml
.definitions:
  .i_am_unreachable_with_extends:
    image: alpine:latest
    timeout: 20 minutes

.i_am_extendable:
  image: !reference [ .definitions, .i_am_unreachable_with_extends, image ]
  timeout: !reference [ .definitions, .i_am_unreachable_with_extends, timeout ]

my-job:
  extends:
    - .i_am_extendable
  script: exit 0
```

<br>

#### **Alternative 1-2**: define a template referencing its source values using YAML anchors:

> This alternative **only works when anchor definition and usage are in the same YAML file**. Learn more about YAML anchors usage [here](https://docs.gitlab.com/ee/ci/yaml/yaml_optimization.html#anchors).
{.is-warning}

```yaml

# This block could be in a separate file...

.definitions:
  .i_am_unreachable_with_extends: &definition_anchor
    image: alpine:latest
    timeout: 20 minutes

.i_am_extendable: *definition_anchor

# ...until here, but both declarations above must be in the same file.

my-job:
  extends:
    - .i_am_extendable
  script: exit 0
```

<br>

#### **Alternative 1-3:** define a template copying data from its source:

> This alternative is **discouraged** from the point of view of sharing data, since definition won't be considered the SSOT anymore. However, it may be useful for customizing a template.
{.is-warning}

```yaml
.definitions:
  .i_am_unreachable_with_extends: &definition_anchor
    image: alpine:latest
    timeout: 20 minutes

.i_am_extendable:
  image: alpine:latest
  timeout: 20 minutes

my-job:
  extends:
    - .i_am_extendable
  script: exit 0
```

<br>

#### ~~**Alternative 1-4**: define a template referencing its full source via `!reference`:~~

> **This is not a real alternative, as it does not work.**
> **It is reflected here just with ilustrative purposes and as a reference of how you cannot do it.**
> **See full explanation in its [dedicated issue](/en/services/gitlab-ci/known-issues#extends-and-reference-together).**
{.is-danger}

```yaml
.definitions:
  .i_am_unreachable_with_extends:
    image: alpine:latest
    timeout: 20 minutes

.i_am_extendable: !reference [ .definitions, .i_am_unreachable_with_extends ]

my-job:
  extends:
    - .i_am_extendable
  script: exit 0
```

<br>

### Alternative 2: Skip template usage

If *Alternative 1* variants do not fit your needs, don't worry, there are still some more available options.

<br>

### Alternative 2 tabset {.tabset}

<br>

#### **Alternative 2-1:** reference directly data from its source:

> This alternative is similar to *Alternative 1-1*, but without intermediate template nor `extends` usage. This alternative is still valid from the point of view of sharing data, since definition may still be considered the SSOT.
{.is-info}

> :x: **[Avoid referencing keys with boolean values](/en/services/gitlab-ci/known-issues#referencing-a-false-value).**
{.is-warning}

```yaml
.definitions:
  .i_am_unreachable_with_extends:
    image: alpine:latest
    timeout: 20 minutes

my-job:
  image: !reference [ .definitions, .i_am_unreachable_with_extends, image ]
  timeout: !reference [ .definitions, .i_am_unreachable_with_extends, timeout ]
  script: exit 0
```

<br>

#### **Alternative 2-2:** copy directly data from its source:

> This alternative is **discouraged** from the point of view of sharing data, since definition won't be considered the SSOT anymore. However, it may be useful for customizing a single job.
{.is-warning}

```yaml
.definitions:
  .i_am_unreachable_with_extends:
    image: alpine:latest
    timeout: 20 minutes

my-job:
  image: alpine:latest
  timeout: 20 minutes
  script: exit 0
```

</details>
