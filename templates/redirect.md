---
title: Redirect
description: Redirect template.
published: true
date: 2021-12-15T17:21:00.428Z
tags: page type - content, section - templates, topic - redirect
editor: markdown
dateCreated: 2021-12-02T17:58:02.428Z
---

Usage:
- Leave the text below as the only page content.
- Remove leading and trailing <kbd>

```</kbd>.
- Put target url where it says `Target url here`, preserving single quotes.

```

> You are being redirected, please wait...
{.is-info}

<img src="/assets/images/redirect/301.png"
  class="elevation-5 radius-5"
  onload="window.location.href='Target url here'"
  />
```
